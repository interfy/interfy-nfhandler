﻿using Newtonsoft.Json;

namespace DS.NFIntegration.App.Entities
{
    public class DownloadRequest : BaseRequest
    {
        [JsonProperty(PropertyName = "ciencia_automatica")]
        public bool Acknowledge { get; set; }

        [JsonProperty(PropertyName = "nsu")]
        public int NSU { get; set; }

        [JsonProperty(PropertyName = "S3")]
        public string Bucket { get; set; }
    }
}
