﻿using DS.NFIntegration.Core.Interfaces;
using Newtonsoft.Json;
using System;
using System.Security.Cryptography.X509Certificates;

namespace DS.NFIntegration.App.Entities
{
    public class BaseRequest
    {
        private X509Certificate2 certificate = null;

        [JsonProperty(PropertyName = "request_id")]
        public string RequestID { get; set; }

        [JsonProperty(PropertyName = "action")]
        public string Action { get; set; }

        [JsonProperty(PropertyName = "cnpj")]
        public string CNPJ { get; set; }

        [JsonProperty(PropertyName = "pfx_file")]
        public string CertificateData { get; set; }

        [JsonProperty(PropertyName = "pass")]
        public string CertificatePassword { get; set; }

        [JsonProperty(PropertyName = "response_uri")]
        public string ResponseUri { get; set; }

        [JsonProperty(PropertyName = "workspace")]
        public string Workspace { get; set; }

        public X509Certificate2 GetCertificate(ILogger logger)
        {
            if (certificate == null)
            {
                try
                {
                    certificate = new X509Certificate2(Convert.FromBase64String(CertificateData), CertificatePassword);
                    if (certificate.NotAfter.Date < DateTime.Now.Date)
                    {
                        if (logger != null)
                            logger.Write(string.Format("Certificado para CNPJ {0} expirou em {1}.", CNPJ, certificate.NotAfter.ToString("dd/MM/yyyy")));

                        certificate = null;
                    }
                }
                catch (Exception ex)
                {
                    if (logger != null)
                        logger.Write(string.Format("Erro obtendo certificado para CNPJ {0} - {1}", CNPJ, ex.Message));

                    certificate = null;
                }
            }

            return certificate;
        }
    }
}
