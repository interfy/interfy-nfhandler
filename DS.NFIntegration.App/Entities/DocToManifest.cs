﻿using Newtonsoft.Json;

namespace DS.NFIntegration.App.Entities
{
    public class DocToManifest
    {
        [JsonProperty(PropertyName = "chNFe")]
        public string Key { get; set; }

        [JsonProperty(PropertyName = "tpEvento")]
        public string EventCode { get; set; }
    }
}
