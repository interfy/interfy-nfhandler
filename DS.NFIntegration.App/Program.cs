﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using Amazon.SQS;
using Amazon.SQS.Model;
using DS.NFIntegration.App.Entities;
using DS.NFIntegration.Core.Business;
using DS.NFIntegration.Core.Entities;
using DS.NFIntegration.Core.Interfaces;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace DS.NFIntegration.App
{
    class Program
    {
        private static IConfiguration configuration;
        private static ILogger logger;

        static void Main(string[] args)
        {
            try
            {
                IConfigurationBuilder builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json");

                configuration = builder.Build();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Erro acessando configuração: {ex.Message}");
                return;
            }

            if (args.Length == 2 && args[0].ToLower() == "sendtotest")
            {
                try
                {
                    SendToTest(args[1]);
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Erro enviando arquivo de teste: {ex.Message}");
                    return;
                }
            }

            try
            {
                logger = new FileLogger();
                ProcessQueue();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Erro não tratado: {ex.Message}");
            }
        }

        /// <summary>
        /// Envia informações de teste para a fila
        /// </summary>
        /// <param name="filePath">Caminho para arquivo contendo dados de teste</param>
        static void SendToTest(string filePath)
        {
            if (!File.Exists(filePath))
                throw new FileNotFoundException($"Arquivo {filePath} não encontrado.");

            Console.WriteLine("Iniciando envio de arquivo teste para fila...");

            string from = configuration["fromQueue"];
            string body = File.ReadAllText(filePath);

            SendMessageRequest req = new SendMessageRequest(from, body);
            req.MessageGroupId = "1";
            req.MessageDeduplicationId = Guid.NewGuid().ToString();

            using (AmazonSQSClient client = new AmazonSQSClient(configuration["aws_key"], configuration["aws_secret"],
                Amazon.RegionEndpoint.GetBySystemName(configuration["aws_region"])))
            {
                SendMessageResponse resp = client.SendMessageAsync(req).Result;
                if (resp.HttpStatusCode == System.Net.HttpStatusCode.OK || resp.HttpStatusCode == System.Net.HttpStatusCode.Created)
                {
                    Console.WriteLine("Item criado com sucesso.");
                }
                else
                {
                    Console.WriteLine($"Item não foi criado:");
                    foreach (KeyValuePair<string, string> entry in resp.ResponseMetadata.Metadata)
                        Console.WriteLine($"\t{entry.Key}: {entry.Value}");
                }
            }
        }

        /// <summary>
        /// Processa uma fila (SQS)
        /// </summary>
        static void ProcessQueue()
        {
            logger.Write("Início do processamento.");

            string from = configuration["fromQueue"];
            string to = configuration["toQueue"];

            if (string.IsNullOrEmpty(from))
                logger.Write("Chave fromQueue não encontrada.");
            else
                logger.Write("Chave(s) encontrada(s). Prosseguindo com processamento.");

            using (AmazonSQSClient client = new AmazonSQSClient(configuration["aws_sqs_key"], 
                configuration["aws_sqs_secret"], Amazon.RegionEndpoint.GetBySystemName(configuration["aws_sqs_region"])))
            {
                SendMessageResponse resp = null;

                // Dependendo do tempo que eu levo para processar uma mensagem, o ReceiptHandler pode expirar.
                // Quando isso ocorre eu não consigo apagar a mensagem. Por isso eu armazendo o MD5 das mensagens
                // que estou processando. Caso se repita, é porque ocorreu esse cenário, e eu posso ignorar a mensagem.
                List<string> messageMD5s = new List<string>();

                ReceiveMessageResponse receiveMessage = client.ReceiveMessageAsync(from).Result;
                logger.Write($"Recebidas {receiveMessage.Messages.Count} mensagens.");

                while(receiveMessage.Messages.Count > 0)
                {
                    foreach (Message msg in receiveMessage.Messages)
                    {
                        try
                        {
                            if(!messageMD5s.Contains(msg.MD5OfBody))
                            {
                                messageMD5s.Add(msg.MD5OfBody);

                                BaseRequest baseRequest = JsonConvert.DeserializeObject<BaseRequest>(msg.Body);
                                Globals.WORKSPACE = baseRequest.Workspace;

                                X509Certificate2 certificate = baseRequest.GetCertificate(logger);
                                if (certificate != null)
                                {
                                    if (baseRequest.Action == "download")
                                    {
                                        DownloadRequest downloadRequest = JsonConvert.DeserializeObject<DownloadRequest>(msg.Body);
                                        NFListRetriever retriever = new NFListRetriever(logger);

                                        string s3Key = configuration["aws_s3_key"];
                                        string s3Secret = configuration["aws_s3_secret"];
                                        string s3Region = configuration["aws_s3_region"];

                                        if (string.IsNullOrEmpty(s3Key) || string.IsNullOrEmpty(s3Secret) || string.IsNullOrEmpty(s3Region))
                                        {
                                            logger.Write("Não encontradas informações de acesso ao S3.");
                                        }
                                        else
                                        {
                                            S3AccessConfiguration s3Access = new S3AccessConfiguration(s3Key, s3Secret, s3Region, downloadRequest.Bucket);

                                            int lastNSU = retriever.Retrieve(downloadRequest.CNPJ, downloadRequest.Acknowledge, downloadRequest.NSU,
                                                s3Access, downloadRequest.ResponseUri, certificate);

                                            // Essa parte foi comentada, porque o token recebido tem validade de 1 hora,
                                            // e não tenho como garantir quando esse item que será adicionado na fila
                                            // será processado.
                                            //if (lastNSU > downloadRequest.NSU)
                                            //{
                                            //    logger.Write($"Último NSU ({lastNSU}) maior do que o inicial ({downloadRequest.NSU}). Reenviando para fila.");
                                            //    downloadRequest.NSU = lastNSU;

                                            //    try
                                            //    {
                                            //        SendMessageRequest req = new SendMessageRequest(from, JsonConvert.SerializeObject(downloadRequest));
                                            //        req.MessageGroupId = "1";
                                            //        req.MessageDeduplicationId = DateTime.Now.Ticks.ToString();

                                            //        resp = client.SendMessageAsync(req).Result;
                                            //        logger.Write("Mensagem reenviada para fila.");
                                            //    }
                                            //    catch (Exception ex)
                                            //    {
                                            //        if (ex.InnerException == null)
                                            //            logger.Write($"Erro reenviando para fila: {ex.Message}");
                                            //        else
                                            //            logger.Write($"Erro reenviando para fila: {ex.InnerException.Message}");
                                            //    }
                                            //}
                                        }
                                    }
                                    else if (baseRequest.Action == "manifest")
                                    {
                                        ManifestRequest manifestRequest = JsonConvert.DeserializeObject<ManifestRequest>(msg.Body);
                                        Manifest manifest = new Manifest(manifestRequest.CNPJ, certificate, manifestRequest.ResponseUri, logger);

                                        foreach (DocToManifest doc in manifestRequest.DocsToManifest)
                                            manifest.ManifestNFe(doc.EventCode, doc.Key, true);
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Write($"Erro processando mensagem: {ex.Message}");
                        }
                        finally
                        {
                            if (!string.IsNullOrEmpty(to))
                            {
                                try
                                {
                                    SendMessageRequest req = new SendMessageRequest(to, msg.Body);
                                    req.MessageGroupId = "1";
                                    req.MessageDeduplicationId = msg.MessageId;

                                    resp = client.SendMessageAsync(req).Result;
                                    logger.Write("Enviada mensagem para fila.");
                                }
                                catch (Exception ex)
                                {
                                    if (ex.InnerException == null)
                                        logger.Write($"Erro enviando para fila: {ex.Message}");
                                    else
                                        logger.Write($"Erro enviando para fila: {ex.InnerException.Message}");
                                }
                            }

                            try
                            {
                                DeleteMessageResponse response = client.DeleteMessageAsync(from, msg.ReceiptHandle).Result;
                                logger.Write("Mensagem original apagada");
                            }
                            catch (Exception ex)
                            {
                                if (ex.InnerException == null)
                                    logger.Write($"Erro apagando mensagem: {ex.Message}");
                                else
                                    logger.Write($"Erro apagando mensagem: {ex.InnerException.Message}");
                            }
                        }
                    }

                    receiveMessage = client.ReceiveMessageAsync(from).Result;
                    logger.Write($"Recebidas {receiveMessage.Messages.Count} mensagens.");
                }
            }

            logger.Write("Processamento finalizado.");
        }
    }
}
