﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DS.NFIntegration.Entities
{
    internal class Environment
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="licenseNumber">Licença</param>
        public Environment(string licenseNumber)
        {
            this.LicenseNumber = licenseNumber;
            this.SharedSecret = false;
            this.Salt = false;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="licenseNumber">Licença</param>
        /// <param name="environment">Nó com informações de configuração</param>
        public Environment(string licenseNumber, XmlNode environment)
        {
            this.LicenseNumber = licenseNumber;
            this.SharedSecret = Hash(licenseNumber) == environment.SelectSingleNode("ss").InnerText;
            this.Salt = Hash(System.Environment.MachineName) == environment.SelectSingleNode("st").InnerText;
        }

        /// <summary>
        /// Número da licença
        /// </summary>
        public string LicenseNumber { get; private set; }

        /// <summary>
        /// Chave ok?
        /// </summary>
        public bool SharedSecret { get; private set; }

        /// <summary>
        /// Salt ok?
        /// </summary>
        public bool Salt { get; private set; }

        /// <summary>
        /// Popula o nó com as configurações atuais
        /// </summary>
        /// <param name="environment">Nó a ser populado</param>
        internal void PopulateNode(XmlNode environment)
        {
            XmlElement ss = environment.OwnerDocument.CreateElement("ss");
            XmlElement st = environment.OwnerDocument.CreateElement("st");

            ss.InnerText = Hash(LicenseNumber);
            st.InnerText = Hash(System.Environment.MachineName);

            environment.AppendChild(ss);
            environment.AppendChild(st);
        }

        private string Hash(string value)
        {
            byte[] data = Encoding.UTF8.GetBytes(value);

            System.Security.Cryptography.SHA1Managed hash = new System.Security.Cryptography.SHA1Managed();
            byte[] tempHash = hash.ComputeHash(data);
            System.Text.StringBuilder sb = new StringBuilder(tempHash.Length * 2);
            foreach (byte b in tempHash)
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }
    }
}
