﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DS.NFIntegration.Entities
{
    /// <summary>
    /// Mapeamento XML - DocSystem
    /// </summary>
    public class MappingItem : ConfigBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="dsConfig">Nó do XML de configuração</param>
        internal MappingItem(XmlNode dsConfig)
        {
            this.NFPath = GetString(dsConfig, "nfPath");
            this.IsFixedValue = GetBool(dsConfig, "isFixedValue");
            this.DSIndexID = GetString(dsConfig, "dsIndexID");
            this.DSIndexName = GetString(dsConfig, "dsIndexName");
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="nfPath">Caminho no XML para o valor desejado</param>
        /// <param name="isFixedValue">Indica se, ao invés de um XPath, o valor de NFPath é um texto fixo</param>
        /// <param name="dsIndexID">ID do índice no DocSystem</param>
        /// <param name="dsIndexName">Nome do índice</param>
        public MappingItem(string nfPath, bool isFixedValue, string dsIndexID, string dsIndexName)
        {
            this.NFPath = nfPath;
            this.IsFixedValue = isFixedValue;
            this.DSIndexID = dsIndexID;
            this.DSIndexName = dsIndexName;
        }

        /// <summary>
        /// Caminho do XML
        /// </summary>
        public string NFPath { get; set; }

        /// <summary>
        /// Indica se, ao invés de um XPath, o valor de NFPath é um texto fixo
        /// </summary>
        public bool IsFixedValue { get; set; }

        /// <summary>
        /// Índice do DocSystem
        /// </summary>
        public string DSIndexID { get; set; }

        /// <summary>
        /// Nome do índice
        /// </summary>
        public string DSIndexName { get; set; }

        /// <summary>
        /// Popula o nó com as configurações atuais
        /// </summary>
        /// <param name="mappingItem">Nó a ser populado</param>
        internal void PopulateNode(XmlNode mappingItem)
        {
            XmlElement nfPath = mappingItem.OwnerDocument.CreateElement("nfPath");
            XmlElement isFixedValue = mappingItem.OwnerDocument.CreateElement("isFixedValue");
            XmlElement dsIndexID = mappingItem.OwnerDocument.CreateElement("dsIndexID");
            XmlElement dsIndexName = mappingItem.OwnerDocument.CreateElement("dsIndexName");

            nfPath.InnerText = this.NFPath;
            isFixedValue.InnerText = this.IsFixedValue ? "true" : "false";
            dsIndexID.InnerText = this.DSIndexID;
            dsIndexName.InnerText = this.DSIndexName;

            mappingItem.AppendChild(nfPath);
            mappingItem.AppendChild(isFixedValue);
            mappingItem.AppendChild(dsIndexID);
            mappingItem.AppendChild(dsIndexName);
        }
    }
}
