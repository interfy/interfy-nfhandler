﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DS.NFIntegration.Entities
{
    public class ConfigBase
    {
        protected string GetString(XmlNode baseNode, string path)
        {
            XmlNode node = baseNode.SelectSingleNode(path);
            if (node == null)
                return string.Empty;

            return node.InnerText;
        }

        protected int GetInt(XmlNode baseNode, string path)
        {
            string value = GetString(baseNode, path);

            int number = 0;
            Int32.TryParse(value, out number);

            return number;
        }

        protected bool GetBool(XmlNode baseNode, string path)
        {
            string value = GetString(baseNode, path);
            return value.ToLower() == "true" || value.ToLower() == "sim";
        }

        protected string GetStringFromAttribute(XmlNode baseNode, string path)
        {
            XmlAttribute att = baseNode.Attributes[path];
            if (att == null)
                return string.Empty;

            return att.InnerText;
        }
    }
}
