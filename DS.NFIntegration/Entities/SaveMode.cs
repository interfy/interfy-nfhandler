﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DS.NFIntegration.Entities
{
    /// <summary>
    /// Modo de salvamento dos arquivos XML e DANFE (PDF)
    /// </summary>
    public enum SaveMode
    {
        /// <summary>
        /// Gravar XML na primeira versão e DANFE na segunda
        /// </summary>
        DifferentVersions = 0,

        /// <summary>
        /// Gravar XML e DANFE juntos na primeira versão
        /// </summary>
        SameVersion = 1,

        /// <summary>
        /// Gravar apenas o XML
        /// </summary>
        OnlyXml = 2
    }
}
