﻿using DS.NFIntegration.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DS.NFIntegration.Entities
{
    /// <summary>
    /// Dados da licença
    /// </summary>
    public class License : ConfigBase
    {
        string salt = string.Empty;

        /// <summary>
        /// Construtor para serialização
        /// </summary>
        public License()
        {
            salt = System.Environment.MachineName;
        }

        internal License(string licenseNumber)
            : this()
        {
            this.LicenseNumber = licenseNumber;
        }

        internal License(XmlNode dsConfig, string licenseNumber)
            : this(licenseNumber)
        {
            this.MaxCompanyIDs = Convert.ToInt32(Crypto.DecryptStringAES(GetString(dsConfig, "maxCompanyIDs"), licenseNumber, salt));
            this.ValidFromString = Crypto.DecryptStringAES(GetString(dsConfig, "validFrom"), licenseNumber, salt);
            this.ValidUntilString = Crypto.DecryptStringAES(GetString(dsConfig, "validUntil"), licenseNumber, salt);
            this.Manifest = Crypto.DecryptStringAES(GetString(dsConfig, "manifest"), licenseNumber, salt) == "true";
        }

        /// <summary>
        /// Número da licença
        /// </summary>
        public string LicenseNumber { get; set; }

        /// <summary>
        /// Número máximo de CNPJs que podem ser cadastrados
        /// </summary>
        public int MaxCompanyIDs { get; set; }

        /// <summary>
        /// Início da validade do serviço
        /// </summary>
        public string ValidFromString { get; set; }

        /// <summary>
        /// Início da validade do serviço
        /// </summary>
        public DateTime ValidFrom 
        { 
            get 
            {
                if (string.IsNullOrEmpty(ValidFromString))
                    return DateTime.MinValue;

                return DateTime.ParseExact(ValidFromString, "dd-MM-yyyy", null);
            }
        }

        /// <summary>
        /// Fim da validade do serviço
        /// </summary>
        public string ValidUntilString { get; set; }

        /// <summary>
        /// Fim da validade do serviço
        /// </summary>
        public DateTime ValidUntil
        {
            get
            {
                if (string.IsNullOrEmpty(ValidUntilString))
                    return DateTime.MinValue;

                return DateTime.ParseExact(ValidUntilString, "dd-MM-yyyy", null);
            }
        }

        /// <summary>
        /// Se a opção de manifesto está disponível
        /// </summary>
        public bool Manifest { get; set; }

        internal void PopulateNode(XmlNode config)
        {
            XmlElement maxCompanyIDs = config.OwnerDocument.CreateElement("maxCompanyIDs");
            XmlElement validFrom = config.OwnerDocument.CreateElement("validFrom");
            XmlElement validUntil = config.OwnerDocument.CreateElement("validUntil");
            XmlElement manifest = config.OwnerDocument.CreateElement("manifest");

            maxCompanyIDs.InnerText = Crypto.EncryptStringAES(MaxCompanyIDs.ToString(), LicenseNumber, salt);
            validFrom.InnerText = Crypto.EncryptStringAES(ValidFromString, LicenseNumber, salt);
            validUntil.InnerText = Crypto.EncryptStringAES(ValidUntilString, LicenseNumber, salt);
            manifest.InnerText = Manifest ? Crypto.EncryptStringAES("true", LicenseNumber, salt) : Crypto.EncryptStringAES("false", LicenseNumber, salt);

            config.AppendChild(maxCompanyIDs);
            config.AppendChild(validFrom);
            config.AppendChild(validUntil);
            config.AppendChild(manifest);
        }
    }
}
