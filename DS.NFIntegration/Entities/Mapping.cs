﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DS.NFIntegration.Entities
{
    /// <summary>
    /// Mapeamento entre XML da receita e DocSystem
    /// </summary>
    public class Mapping : ConfigBase
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="mapping">Nó do XML de configuração</param>
        internal Mapping(XmlNode mapping)
        {
            this.CabinetID = GetStringFromAttribute(mapping, "cabinetID");
            this.CabinetName = GetStringFromAttribute(mapping, "cabinetName");
            this.DocTypeID = GetStringFromAttribute(mapping, "docTypeID");
            this.DocTypeName = GetStringFromAttribute(mapping, "docTypeName");
            this.DestCNPJIndexId = GetStringFromAttribute(mapping, "destCNPJIndexId");
            this.NFeKeyIndexId = GetStringFromAttribute(mapping, "nfeKeyIndexId");
            this.ManifestStatusIndexId = GetStringFromAttribute(mapping, "manifestStatusIndexId");
            this.NfeDownloadDateIndexId = GetStringFromAttribute(mapping, "nfeDownloadDateIndexId");
            this.NfeDownloadMessageStatusIndexId = GetStringFromAttribute(mapping, "nfeDownloadMessageStatusIndexId");
            this.Items = new List<MappingItem>();

            foreach (XmlNode item in mapping.SelectNodes("item"))
                this.Items.Add(new MappingItem(item));
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="cabinetID">ID do repositório</param>
        /// <param name="cabinetName">Nome do repositório</param>
        /// <param name="docTypeID">ID do tipo de documento</param>
        /// <param name="docTypeName">Nome do tipo de documento</param>
        /// <param name="destCNPJIndexId">CNPJ de destino da NFe</param>
        /// <param name="nfeKeyIndexId">ID do índice relativo à chave da NFe</param>
        /// <param name="manifestStatusIndexId">ID do índice relativo ao status do manifesto</param>
        /// <param name="nfeDownloadDateIndexId">ID do índice relativo ao download da nota</param>
        /// <param name="nfeDownloadMessageStatusIndexId">ID do índice relativo à mensagem de status de download</param>
        public Mapping(string cabinetID, string cabinetName, string docTypeID, string docTypeName,
            string destCNPJIndexId, string nfeKeyIndexId, string manifestStatusIndexId, string nfeDownloadDateIndexId,
            string nfeDownloadMessageStatusIndexId)
        {
            this.CabinetID = cabinetID;
            this.CabinetName = cabinetName;
            this.DocTypeID = docTypeID;
            this.DocTypeName = docTypeName;
            this.DestCNPJIndexId = destCNPJIndexId;
            this.NFeKeyIndexId = nfeKeyIndexId;
            this.ManifestStatusIndexId = manifestStatusIndexId;
            this.NfeDownloadDateIndexId = nfeDownloadDateIndexId;
            this.NfeDownloadMessageStatusIndexId = nfeDownloadMessageStatusIndexId;
            this.Items = new List<MappingItem>();
        }

        /// <summary>
        /// ID do repositório
        /// </summary>
        public string CabinetID { get; set; }

        /// <summary>
        /// Nome do repositório
        /// </summary>
        public string  CabinetName { get; set; }

        /// <summary>
        /// ID do tipo de documento
        /// </summary>
        public string DocTypeID { get; set; }

        /// <summary>
        /// Nome do tipo de documento
        /// </summary>
        public string DocTypeName { get; set; }

        /// <summary>
        /// ID do índice relativo ao CNPJ de destino da NFe
        /// </summary>
        public string DestCNPJIndexId { get; set; }

        /// <summary>
        /// ID do índice relativo à chave da NFe
        /// </summary>
        public string NFeKeyIndexId { get; set; }

        /// <summary>
        /// ID do índice relativo ao status do manifesto
        /// </summary>
        public string ManifestStatusIndexId { get; set; }

        /// <summary>
        /// ID do índice relativo ao download da nota
        /// </summary>
        public string NfeDownloadDateIndexId { get; set; }

        /// <summary>
        /// ID do índice relativo à mensagem de status de download
        /// </summary>
        public string NfeDownloadMessageStatusIndexId { get; set; }

        /// <summary>
        /// Mapeamento dos campos
        /// </summary>
        public List<MappingItem> Items { get; set; }

        /// <summary>
        /// Popula o nó com as configurações atuais
        /// </summary>
        /// <param name="mapping">Nó a ser populado</param>
        internal void PopulateNode(XmlNode mapping)
        {
            XmlAttribute cabinetID = mapping.OwnerDocument.CreateAttribute("cabinetID");
            XmlAttribute cabinetName = mapping.OwnerDocument.CreateAttribute("cabinetName");
            XmlAttribute docTypeID = mapping.OwnerDocument.CreateAttribute("docTypeID");
            XmlAttribute docTypeName = mapping.OwnerDocument.CreateAttribute("docTypeName");
            XmlAttribute destCNPJIndexId = mapping.OwnerDocument.CreateAttribute("destCNPJIndexId");
            XmlAttribute nfeKeyIndexId = mapping.OwnerDocument.CreateAttribute("nfeKeyIndexId");
            XmlAttribute manifestStatusIndexId = mapping.OwnerDocument.CreateAttribute("manifestStatusIndexId");
            XmlAttribute nfeDownloadDateIndexId = mapping.OwnerDocument.CreateAttribute("nfeDownloadDateIndexId");
            XmlAttribute nfeDownloadMessageStatusIndexId = mapping.OwnerDocument.CreateAttribute("nfeDownloadMessageStatusIndexId");

            cabinetID.InnerText = this.CabinetID;
            cabinetName.InnerText = this.CabinetName;
            docTypeID.InnerText = this.DocTypeID;
            docTypeName.InnerText = this.DocTypeName;
            destCNPJIndexId.InnerText = this.DestCNPJIndexId;
            nfeKeyIndexId.InnerText = this.NFeKeyIndexId;
            manifestStatusIndexId.InnerText = this.ManifestStatusIndexId;
            nfeDownloadDateIndexId.InnerText = this.NfeDownloadDateIndexId;
            nfeDownloadMessageStatusIndexId.InnerText = this.NfeDownloadMessageStatusIndexId;

            mapping.Attributes.Append(cabinetID);
            mapping.Attributes.Append(cabinetName);
            mapping.Attributes.Append(docTypeID);
            mapping.Attributes.Append(docTypeName);
            mapping.Attributes.Append(destCNPJIndexId);
            mapping.Attributes.Append(nfeKeyIndexId);
            mapping.Attributes.Append(manifestStatusIndexId);
            mapping.Attributes.Append(nfeDownloadDateIndexId);
            mapping.Attributes.Append(nfeDownloadMessageStatusIndexId);

            foreach(MappingItem item in this.Items)
            {
                XmlElement newItem = mapping.OwnerDocument.CreateElement("item");
                item.PopulateNode(newItem);

                mapping.AppendChild(newItem);
            }
        }
    }
}
