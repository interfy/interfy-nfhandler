﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DS.NFIntegration.Entities
{
    /// <summary>
    /// Status de manifesto
    /// </summary>
    public enum ManifestStatus
    {
        /// <summary>
        /// Sem manifestação
        /// </summary>
        Pending = 0,

        /// <summary>
        /// Confirmado
        /// </summary>
        Confirmed = 1,

        /// <summary>
        /// Desconhecido
        /// </summary>
        Unknown = 2,

        /// <summary>
        /// Recusado
        /// </summary>
        Refused = 3,

        /// <summary>
        /// Ciência
        /// </summary>
        Known = 4
    }
}
