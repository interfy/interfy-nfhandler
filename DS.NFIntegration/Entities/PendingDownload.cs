﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DS.NFIntegration.Entities
{
    internal class PendingDownload
    {
        public PendingDownload(string docGUID, string key, ManifestStatus currentManifestStatus, NFConfiguration nfConfig)
        {
            this.DocGUID = docGUID;
            this.Key = key;
            this.CurrentManifestStatus = currentManifestStatus;
            this.NFConfig = nfConfig;
        }

        public string DocGUID { get; set; }

        public string Key { get; private set; }

        public ManifestStatus CurrentManifestStatus { get; set; }

        public NFConfiguration NFConfig { get; private set; }
    }
}
