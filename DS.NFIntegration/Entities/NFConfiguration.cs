﻿using DS.NFIntegration.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DS.NFIntegration.Entities
{
    /// <summary>
    /// Configuração para recuperação de dados de NF
    /// </summary>
    public class NFConfiguration : ConfigBase
    {
        string salt = string.Empty;

        private NFConfiguration(string licenseNumber)
        {
            this.salt = System.Environment.MachineName;
            this.LicenseNumber = licenseNumber;
        }

        internal NFConfiguration(XmlNode nfConfig, string licenseNumber)
            : this(licenseNumber)
        {
            CabinetID = GetStringFromAttribute(nfConfig, "cabinetID");
            CNPJ = GetString(nfConfig, "cnpj");
            Certificate = GetString(nfConfig, "certificate");
            StateCode = GetString(nfConfig, "stateCode");
            SaveMode = (Entities.SaveMode)(GetInt(nfConfig, "saveMode"));
            AutomaticManifest = GetBool(nfConfig, "automaticManifest");
            LastNSU = GetString(nfConfig, "lastNSU");

            Password = GetString(nfConfig, "password");
            if (!string.IsNullOrEmpty(Password))
                Password = Crypto.DecryptStringAES(Password, licenseNumber, salt);
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="cabinetID">ID do repositório</param>
        /// <param name="cnpj">CNPJ</param>
        /// <param name="certificate">Caminho para o certificado</param>
        /// <param name="password">Senha do certificado (se estiver em branco, é para usar certificado instalado e não arquivo)</param>
        /// <param name="stateCode">UF</param>
        /// <param name="saveMode">Modo de gravação</param>
        /// <param name="lastNSU">Último NSU pesquisado</param>
        /// <param name="licenseNumber">Licença</param>
        /// <param name="automaticManifest">Indica se o manifesto deve ser feito automaticamente</param>
        public NFConfiguration(string cabinetID, string cnpj, string certificate, string password, string stateCode,
            SaveMode saveMode, string lastNSU, string licenseNumber, bool automaticManifest)
            : this(licenseNumber)
        {
            this.CabinetID = cabinetID;
            this.CNPJ = cnpj;
            this.Certificate = certificate;
            this.Password = password;
            this.StateCode = stateCode;
            this.SaveMode = saveMode;
            this.LastNSU = lastNSU;
            this.AutomaticManifest = automaticManifest;
        }

        /// <summary>
        /// Número da licença
        /// </summary>
        public string LicenseNumber { get; set; }

        /// <summary>
        /// ID do repositório
        /// </summary>
        public string CabinetID { get; set; }

        /// <summary>
        /// CNPJ
        /// </summary>
        public string CNPJ { get; set; }

        /// <summary>
        /// Caminho para o certificado
        /// </summary>
        public string Certificate { get; set; }

        /// <summary>
        /// Senha do certificado (se estiver em branco, é para usar certificado instalado e não arquivo)
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// UF
        /// </summary>
        public string StateCode { get; set; }

        /// <summary>
        /// Modo de gravação
        /// </summary>
        public SaveMode SaveMode { get; set; }

        /// <summary>
        /// Indica se o manifesto deve ser feito automaticamente
        /// </summary>
        public bool AutomaticManifest { get; set; }

        /// <summary>
        /// Último NSU pesquisado
        /// </summary>
        public string LastNSU { get; set; }

        /// <summary>
        /// Popula o nó com as configurações atuais
        /// </summary>
        /// <param name="nfConfig">Nó a ser populado</param>
        internal void PopulateNode(XmlNode nfConfig)
        {
            XmlAttribute cabinetID = nfConfig.OwnerDocument.CreateAttribute("cabinetID");
            cabinetID.InnerText = this.CabinetID;
            nfConfig.Attributes.Append(cabinetID);

            XmlElement cnpj = nfConfig.OwnerDocument.CreateElement("cnpj");
            XmlElement certificate = nfConfig.OwnerDocument.CreateElement("certificate");
            XmlElement password = nfConfig.OwnerDocument.CreateElement("password");
            XmlElement stateCode = nfConfig.OwnerDocument.CreateElement("stateCode");
            XmlElement saveMode = nfConfig.OwnerDocument.CreateElement("saveMode");
            XmlElement lastNSU = nfConfig.OwnerDocument.CreateElement("lastNSU");
            XmlElement automaticManifest = nfConfig.OwnerDocument.CreateElement("automaticManifest");

            cnpj.InnerText = this.CNPJ;
            certificate.InnerText = this.Certificate;

            if (!string.IsNullOrEmpty(Password))
                password.InnerText = Crypto.EncryptStringAES(Password, LicenseNumber, salt);

            stateCode.InnerText = this.StateCode;
            saveMode.InnerText = (Convert.ToInt32(this.SaveMode)).ToString();
            lastNSU.InnerText = this.LastNSU;
            automaticManifest.InnerText = this.AutomaticManifest.ToString();

            nfConfig.AppendChild(cnpj);
            nfConfig.AppendChild(certificate);
            nfConfig.AppendChild(password);
            nfConfig.AppendChild(stateCode);
            nfConfig.AppendChild(saveMode);
            nfConfig.AppendChild(lastNSU);
            nfConfig.AppendChild(automaticManifest);
        }

        #region Propriedades dinâmicas

        /// <summary>
        /// Número do estado, de acordo com o seu código
        /// </summary>
        internal int StateNumber
        {
            get
            {
                switch (StateCode.ToUpper())
                {
                    case "AC":
                        return 12;
                    case "AL":
                        return 27;
                    case "AP":
                        return 16;
                    case "AM":
                        return 13;
                    case "BA":
                        return 29;
                    case "CE":
                        return 23;
                    case "DF":
                        return 53;
                    case "ES":
                        return 32;
                    case "GO":
                        return 52;
                    case "MA":
                        return 21;
                    case "MT":
                        return 51;
                    case "MS":
                        return 50;
                    case "MG":
                        return 31;
                    case "PA":
                        return 15;
                    case "PB":
                        return 25;
                    case "PR":
                        return 41;
                    case "PE":
                        return 26;
                    case "PI":
                        return 22;
                    case "RJ":
                        return 33;
                    case "RN":
                        return 24;
                    case "RS":
                        return 43;
                    case "RO":
                        return 11;
                    case "RR":
                        return 14;
                    case "SC":
                        return 42;
                    case "SE":
                        return 28;
                    case "TO":
                        return 17;
                    default:
                        return 35;
                }
            }
        }

        /// <summary>
        /// URL para web service de download de NFe
        /// </summary>
        internal string DownloadNFeUrl
        {
            get
            {
                //switch (StateCode.ToUpper())
                //{
                //    case "CE":
                //        return "https://nfe.sefaz.ce.gov.br/nfe2/services/NfeDownloadNF";

                //    case "MA":
                //    case "PA":
                //    case "PI":
                //        return "https://www.sefazvirtual.fazenda.gov.br/NfeDownloadNF/NfeDownloadNF.asmx";

                //    case "RS":
                //        return "https://nfe.sefazrs.rs.gov.br/ws/nfeDownloadNF/nfeDownloadNF.asmx";

                //    default:
                //        return "https://www.nfe.fazenda.gov.br/NfeDownloadNF/NfeDownloadNF.asmx";
                //}

                switch (StateCode.ToUpper())
                {
                    case "CE":
                        return "https://nfe.sefaz.ce.gov.br/nfe2/services/NfeDownloadNF";

                    default:
                        return "https://www.sefazvirtual.fazenda.gov.br/NfeDownloadNF/NfeDownloadNF.asmx";
                }
            }
        }

        //internal string EventReceptionUrl
        //{
        //    get
        //    {
        //        //switch (StateCode.ToUpper())
        //        //{
        //        //    case "AM":
        //        //        return "https://nfe.sefaz.am.gov.br/services2/services/RecepcaoEvento";
        //        //    case "BA":
        //        //        return "https://nfe.sefaz.ba.gov.br/webservices/sre/recepcaoevento.asmx";
        //        //    case "CE":
        //        //        return "https://nfe.sefaz.ce.gov.br/nfe2/services/RecepcaoEvento";
        //        //    case "GO":
        //        //        return "https://nfe.sefaz.go.gov.br/nfe/services/v2/RecepcaoEvento";
        //        //    case "MG":
        //        //        return "https://nfe.fazenda.mg.gov.br/nfe2/services/RecepcaoEvento";
        //        //    case "MS":
        //        //        return "https://nfe.fazenda.ms.gov.br/producao/services2/RecepcaoEvento";
        //        //    case "MT":
        //        //        return "https://nfe.sefaz.mt.gov.br/nfews/v2/services/RecepcaoEvento";
        //        //    case "PE":
        //        //        return "https://nfe.sefaz.pe.gov.br/nfe-service/services/RecepcaoEvento";
        //        //    case "PR":
        //        //        return "https://nfe.fazenda.pr.gov.br/nfe/NFeRecepcaoEvento";
        //        //    case "RS":
        //        //        return "https://nfe.sefazrs.rs.gov.br/ws/recepcaoevento/recepcaoevento.asmx";
        //        //    case "SP":
        //        //        return "https://nfe.fazenda.sp.gov.br/ws/recepcaoevento.asmx";

        //        //    case "MA":
        //        //    case "PA":
        //        //    case "PI":
        //        //        return "https://www.sefazvirtual.fazenda.gov.br/RecepcaoEvento/RecepcaoEvento.asmx";
                    
        //        //    default:
        //        //        return "https://nfe.svrs.rs.gov.br/ws/recepcaoevento/recepcaoevento.asmx";
        //        //}
        //    }
        //}

        #endregion
    }
}
