﻿using DS.NFIntegration.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DS.NFIntegration.Entities
{
    /// <summary>
    /// Configuração para acesso ao DocSystem
    /// </summary>
    public class DSConfiguration : ConfigBase
    {
        string salt = string.Empty;
        string originalPassword = string.Empty;

        private DSConfiguration()
        {
            this.salt = System.Environment.MachineName;
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="dsConfig">Nó do XML de configuração</param>
        internal DSConfiguration(XmlNode dsConfig)
            : this()
        {
            this.BaseUrl = GetString(dsConfig, "baseUrl");
            this.Domain = GetString(dsConfig, "domain");
            this.UserName = GetString(dsConfig, "userName");
            this.Password = string.Empty;
            this.originalPassword = GetString(dsConfig, "password");
            this.LicenseNumber = GetString(dsConfig, "licenseNumber");

            XmlNode licenseUsage = dsConfig.SelectSingleNode("licenseUsage");
            if (licenseUsage != null)
                this.LicenceUsage = new License(licenseUsage, LicenseNumber);
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="baseUrl">Url base</param>
        /// <param name="domain">Domínio</param>
        /// <param name="userName">Usuário</param>
        /// <param name="password">Senha</param>
        /// <param name="licenseNumber">Número da lincença</param>
        public DSConfiguration(string baseUrl, string domain, string userName, string password, string licenseNumber)
            : this()
        {
            this.BaseUrl = baseUrl;
            this.Domain = domain;
            this.UserName = userName;
            this.Password = password;
            this.LicenseNumber = licenseNumber;
            this.LicenceUsage = new License(licenseNumber);
        }

        /// <summary>
        /// Url base
        /// </summary>
        public string BaseUrl { get; set; }

        /// <summary>
        /// Domínio
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        /// Usuário
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Senha
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Número da licença
        /// </summary>
        public string LicenseNumber { get; set; }

        /// <summary>
        /// Senha original 
        /// </summary>
        internal string OriginalPassword
        {
            get
            {
                if (string.IsNullOrEmpty(originalPassword))
                    return Password;
                else
                    return Crypto.DecryptStringAES(originalPassword, LicenseNumber, salt);
            }
        }

        /// <summary>
        /// Permissões da licença
        /// </summary>
        public License LicenceUsage { get; set; }

        /// <summary>
        /// Popula o nó com as configurações atuais
        /// </summary>
        /// <param name="dsConfig">Nó a ser populado</param>
        internal void PopulateNode(XmlNode dsConfig)
        {
            XmlElement licenseNumber = dsConfig.OwnerDocument.CreateElement("licenseNumber");
            XmlElement baseUrl = dsConfig.OwnerDocument.CreateElement("baseUrl");
            XmlElement domain = dsConfig.OwnerDocument.CreateElement("domain");
            XmlElement userName = dsConfig.OwnerDocument.CreateElement("userName");
            XmlElement password = dsConfig.OwnerDocument.CreateElement("password");

            licenseNumber.InnerText = LicenseNumber;
            baseUrl.InnerText = BaseUrl;
            domain.InnerText = Domain;
            userName.InnerText = UserName;

            if (!string.IsNullOrEmpty(originalPassword) && string.IsNullOrEmpty(Password))
            {
                // Senha não mudou
                password.InnerText = originalPassword;
            }
            else
            {
                // Senha mudou
                password.InnerText = Crypto.EncryptStringAES(this.Password, this.LicenseNumber, salt);
            }

            dsConfig.AppendChild(licenseNumber);
            dsConfig.AppendChild(baseUrl);
            dsConfig.AppendChild(domain);
            dsConfig.AppendChild(userName);
            dsConfig.AppendChild(password);

            XmlElement licenseUsage = dsConfig.OwnerDocument.CreateElement("licenseUsage");
            LicenceUsage.PopulateNode(licenseUsage);
            dsConfig.AppendChild(licenseUsage);
        }
    }
}
