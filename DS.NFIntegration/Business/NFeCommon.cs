﻿using DS.NFIntegration.Entities;
using DS.NFIntegration.Exceptions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace DS.NFIntegration.Business
{
    internal class NFeCommon : IDisposable
    {
        //private DocSystem docsystem = null;
        //private Configuration config = null;

        public NFeCommon(object config)
        {
            //if (config != null)
            //{
            //    this.config = config;

            //    docsystem = new DocSystem(config.DSConfiguration.Domain, config.DSConfiguration.UserName,
            //        config.DSConfiguration.OriginalPassword, string.Empty, string.Empty, string.Empty,
            //        string.Empty, config.DSConfiguration.BaseUrl);
            //}
        }

        /// <summary>
        /// Descompacta XML retornado
        /// </summary>
        /// <param name="content">Conteúdo compactado</param>
        /// <returns>XmlDocument</returns>
        public XmlDocument UncompressDocument(string content)
        {
            byte[] compressedContent = Convert.FromBase64String(content);
            List<byte> uncompressedContent = new List<byte>();

            using (MemoryStream stream = new MemoryStream(compressedContent))
            {
                using (GZipStream zip = new GZipStream(stream, CompressionMode.Decompress))
                {
                    zip.BaseStream.Seek(0, SeekOrigin.Begin);

                    int singleByte = zip.ReadByte();
                    while (singleByte >= 0)
                    {
                        uncompressedContent.Add(Convert.ToByte(singleByte));
                        singleByte = zip.ReadByte();
                    }
                }
            }

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(Encoding.UTF8.GetString(uncompressedContent.ToArray()));

            return doc;
        }

        /// <summary>
        /// Retorna documento do DocSystem completo
        /// </summary>
        /// <param name="nfeDoc">XML da nota</param>
        /// <param name="cnpj">CNPJ</param>
        /// <param name="manifestStatus">Status do manifesto</param>
        /// <param name="existingDocGUID">ID existente no DocSystem</param>
        /// <returns></returns>
        public void GetCompleteDocument(XmlDocument nfeDoc, string cnpj, ManifestStatus manifestStatus, string existingDocGUID)
        {
            Mapping mapping = null;// config.Mappings[config.NFConfigurations[cnpj].CabinetID];

            XmlNamespaceManager ns = new XmlNamespaceManager(nfeDoc.NameTable);
            ns.AddNamespace("nfens", "http://www.portalfiscal.inf.br/nfe");

            Dictionary<string, object> indexFields = new Dictionary<string, object>();

            indexFields.Add(mapping.DestCNPJIndexId, cnpj);
            indexFields.Add(mapping.NFeKeyIndexId, nfeDoc.DocumentElement.SelectSingleNode("//nfens:infNFe/@Id", ns).InnerText.Replace("NFe", string.Empty));
            indexFields.Add(mapping.ManifestStatusIndexId, (Convert.ToInt32(manifestStatus)).ToString());
            indexFields.Add(mapping.NfeDownloadDateIndexId, DateTime.Now);

            if (!string.IsNullOrEmpty(mapping.NfeDownloadMessageStatusIndexId))
                indexFields.Add(mapping.NfeDownloadMessageStatusIndexId, "Nota baixada com sucesso.");

            foreach (MappingItem item in mapping.Items)
            {
                if (indexFields.ContainsKey(item.DSIndexID))
                    continue;

                if (item.IsFixedValue)
                {
                    indexFields.Add(item.DSIndexID, item.NFPath);
                }
                else
                {
                    XmlNode node = nfeDoc.DocumentElement.SelectSingleNode(item.NFPath, ns);

                    if (node != null && !string.IsNullOrEmpty(node.InnerText))
                    {
                        // Tratamento para campo data
                        DateTime date = DateTime.MinValue;
                        Decimal number = 0;

                        if (IsDate(node.InnerText, out date))
                            indexFields.Add(item.DSIndexID, date);
                        else if (IsDecimal(node.InnerText, out number))
                            indexFields.Add(item.DSIndexID, number);
                        else
                            indexFields.Add(item.DSIndexID, node.InnerText);
                    }
                }
            }

            if (string.IsNullOrEmpty(existingDocGUID))
            {
                // Antes de retornar, verifico se existe um documento com a mesma chave da nota.
                // Como esta informação é única, se existir, posso atualizar o documento.
                //Document[] docs = docsystem.Documents.GetDocuments(mapping.CabinetID, mapping.DocTypeID, mapping.NFeKeyIndexId, indexFields[mapping.NFeKeyIndexId].ToString());

                //if (docs == null || docs.Length == 0)
                //    return Document.CreateNew(mapping.CabinetID, mapping.DocTypeID, indexFields);
                //else
                //    return Document.CreateNew(mapping.CabinetID, mapping.DocTypeID, indexFields, new Guid(docs[0].GUID));
            }
            else
            {
                //return Document.CreateNew(mapping.CabinetID, mapping.DocTypeID, indexFields, new Guid(existingDocGUID));
            }
        }

        /// <summary>
        /// Salva nova no DocSystem
        /// </summary>
        /// <param name="doc">Documento</param>
        /// <param name="nfeDoc">XML da nota</param>
        /// <param name="cnpj">CNPJ</param>
        /// <param name="baseDirectory">Diretório base da execução</param>
        public void SaveCompleteNFe(object doc, XmlDocument nfeDoc, string cnpj, string baseDirectory)
        {
            
        }

        private bool IsDate(string value, out DateTime date)
        {
            date = DateTime.MinValue;

            if (string.IsNullOrEmpty(value))
                return false;
            else if (value.Length < 10)
                return false;

            // Regex para formato yyyy-MM-dd sendo ano > 201X e no início da string
            Regex rx = new Regex("(?<Date>^20[1-9][0-9]-[0-1][0-9]-[0-3][0-9])");
            MatchCollection matches = rx.Matches(value);
            if (matches.Count > 0)
            {
                date = DateTime.ParseExact(value.Substring(0, 10), "yyyy-MM-dd", null);
                return true;
            }

            return false;
        }

        private bool IsDecimal(string value, out Decimal number)
        {
            number = 0;

            if (string.IsNullOrEmpty(value))
                return false;

            Regex rx = new Regex(@"^\d+\.\d{2,4}");
            MatchCollection matches = rx.Matches(value);
            if (matches.Count > 0 && matches[0].Length == value.Length)
            {
                number = Decimal.Parse(value, new CultureInfo("en-US"));
                return true;
            }

            return false;
        }

        public void Dispose()
        {
            //if(docsystem != null)
            //{
            //    docsystem.Dispose();
            //    docsystem = null;
            //}
        }
    }
}
