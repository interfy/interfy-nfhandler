﻿using DS.NFIntegration.Entities;
using DS.NFIntegration.Exceptions;
using DS.NFIntegration.SEFAZ.NFeDistribuicaoDFe;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;

namespace DS.NFIntegration.Business
{
    public class NFDownloader
    {
        /// <summary>
        /// Baixa as notas que já foram manifestadas no DocSystem
        /// </summary>
        public void DownloadPendingNFes()
        {
            string logPath = Path.Combine(Directory.GetCurrentDirectory(), "Log");
            if (!Directory.Exists(logPath))
                Directory.CreateDirectory(logPath);

            //Log log = null;

            try
            {
                //log = new Log(logPath);
                //log.Write("DownloadPendingNFes - Processamento iniciado...", true);

                // Pegando todos os certificados cadastrados
                Dictionary<string, X509Certificate2> certificates = new Dictionary<string, X509Certificate2>();

                //foreach (string cnpj in config.NFConfigurations.Keys)
                //{
                //    try
                //    {
                //        X509Certificate2 certificate = config.GetCertificate(cnpj);
                //        if (certificate == null)
                //            log.Write(string.Format("DownloadPendingNFes - Nenhum certificado retornado para CNPJ {0}", cnpj), true);
                //        else
                //            certificates.Add(cnpj, certificate);
                //    }
                //    catch (Exception ex)
                //    {
                //        log.Write(string.Format("DownloadPendingNFes - Erro obtendo certificado para CNPJ {0}", cnpj), ex, true);
                //        return;
                //    }
                //}

                // Pegando todas as notas manifestadas e que estão pendentes para download
                PendingDownload[] pendingRequests = null;

                try
                {
                    //pendingRequests = GetPendingNFeRequests(config);
                    if (pendingRequests == null || pendingRequests.Length == 0)
                        return;
                }
                catch (Exception ex)
                {
                    //log.Write("DownloadPendingNFes - Erro buscando lista de notas pendentes de download", ex, true);
                    return;
                }

                using (NFeCommon common = new NFeCommon(null))
                {
                    // Baixando as notas
                    foreach (PendingDownload request in pendingRequests)
                    {
                        if (!certificates.ContainsKey(request.NFConfig.CNPJ))
                            continue;

                        XmlDocument nfeDocument = null;
                        bool discard = false;
                        string discardReason = string.Empty;

                        try
                        {
                            nfeDocument = DownloadNFe(request.Key, DateTime.MinValue, request.NFConfig.StateNumber.ToString(),
                                request.NFConfig.CNPJ, certificates[request.NFConfig.CNPJ], null, out discard, out discardReason);
                        }
                        catch (Exception ex)
                        {
                            string message = string.Format("DownloadPendingNFes ({0}) - Erro baixando nota", request.Key);
                            //log.Write(message, ex, true);
                            continue;
                        }

                        if (discard)
                        {
                            // A nota não será mais baixada. Então não há porque tentar novamente.
                            //using (DocSystem docsystem = new DocSystem(config.DSConfiguration.Domain, config.DSConfiguration.UserName,
                            //    config.DSConfiguration.OriginalPassword, string.Empty, string.Empty, string.Empty, string.Empty, config.DSConfiguration.BaseUrl))
                            //{
                            //    Mapping mapping = config.Mappings[config.NFConfigurations[request.NFConfig.CNPJ].CabinetID];

                            //    Document doc = docsystem.Documents.GetDocuments(mapping.CabinetID, mapping.DocTypeID, "GUID", request.DocGUID)[0];

                            //    if (doc.IndexFields.ContainsKey("FD_" + mapping.NfeDownloadDateIndexId))
                            //        doc.IndexFields["FD_" + mapping.NfeDownloadDateIndexId] = DateTime.Now;
                            //    else
                            //        doc.IndexFields.Add("FD_" + mapping.NfeDownloadDateIndexId, DateTime.Now);

                            //    if (!string.IsNullOrEmpty(discardReason))
                            //    {
                            //        if (doc.IndexFields.ContainsKey("FD_" + mapping.NfeDownloadMessageStatusIndexId))
                            //            doc.IndexFields["FD_" + mapping.NfeDownloadMessageStatusIndexId] = discardReason;
                            //        else
                            //            doc.IndexFields.Add("FD_" + mapping.NfeDownloadMessageStatusIndexId, discardReason);
                            //    }

                            //    docsystem.Documents.UpdateDocument(doc);
                            //}
                        }
                        else
                        {
                            XmlNamespaceManager ns = new XmlNamespaceManager(nfeDocument.NameTable);
                            ns.AddNamespace("nfens", "http://www.portalfiscal.inf.br/nfe");

                            XmlDocument completeNfe = new XmlDocument();
                            completeNfe.LoadXml(nfeDocument.DocumentElement.SelectSingleNode("//nfens:NFe", ns).OuterXml);

                            try
                            {
                                //Document doc = common.GetCompleteDocument(completeNfe, request.NFConfig.CNPJ, request.CurrentManifestStatus, request.DocGUID);
                                //common.SaveCompleteNFe(doc, completeNfe, request.NFConfig.CNPJ, Directory.GetCurrentDirectory());
                            }
                            catch (UpdateDocumentException ex)
                            {
                                //log.Write(string.Format("DownloadPendingNFes ({0}) - Erro gravando nota", request.Key), true);

                                //foreach (KeyValuePair<string, object> updateDocEntry in ex.Document.IndexFields)
                                //    log.Write(string.Format("\t{0}: {1}", updateDocEntry.Key, (updateDocEntry.Value == null ? string.Empty : updateDocEntry.Value.ToString())), true);

                                //log.Write(ex.Message, true);
                            }
                            catch (Exception ex)
                            {
                                string message = string.Format("DownloadPendingNFes ({0}) - Erro gravando nota", request.Key);
                                //log.Write(message, ex, true);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //if (log != null)
                //    log.Write("DownloadPendingNFes - Erro não tratado", ex, true);
            }
            finally
            {
                //if (log != null)
                //{
                //    log.Write("DownloadPendingNFes - Processamento finalizado.", true);
                //    log.Dispose();
                //    log = null;
                //}
            }
        }

        /// <summary>
        /// Retorna documentos pendentes de download
        /// </summary>
        /// <param name="config">Configuração geral</param>
        /// <returns>PendingDownload[]</returns>
        //private PendingDownload[] GetPendingNFeRequests(Configuration config)
        //{
        //    // Lista de tipos de documento já buscados (evita busca repetida)
        //    List<string> searchedCabinetsAndDocTypes = new List<string>();

        //    // Lista de requests para a SEFAZ
        //    Dictionary<string, PendingDownload> requests = new Dictionary<string, PendingDownload>();

        //    using (DocSystem docsystem = new DocSystem(config.DSConfiguration.Domain, config.DSConfiguration.UserName,
        //        config.DSConfiguration.OriginalPassword, string.Empty, string.Empty, string.Empty, string.Empty, config.DSConfiguration.BaseUrl))
        //    {
        //        foreach (KeyValuePair<string, Mapping> mapping in config.Mappings)
        //        {
        //            string searchKey = string.Format("{0}_{1}", mapping.Value.CabinetID, mapping.Value.DocTypeID);

        //            if (searchedCabinetsAndDocTypes.IndexOf(searchKey) >= 0)
        //                continue;
        //            else
        //                searchedCabinetsAndDocTypes.Add(searchKey);

        //            // Baixando notas com ciência
        //            SearchParameter[] searchParams = new SearchParameter[2];
        //            searchParams[0] = new SearchParameter(SearchOperator.Equal, mapping.Value.ManifestStatusIndexId, (Convert.ToInt32(ManifestStatus.Known)).ToString(), string.Empty);
        //            searchParams[1] = new SearchParameter(SearchOperator.Empty, mapping.Value.NfeDownloadDateIndexId, string.Empty, string.Empty);
        //            List<Document> docs = new List<Document>(docsystem.Documents.GetDocuments(mapping.Value.CabinetID, mapping.Value.DocTypeID, searchParams));

        //            // Baixando notas confirmadas
        //            searchParams[0] = new SearchParameter(SearchOperator.Equal, mapping.Value.ManifestStatusIndexId, (Convert.ToInt32(ManifestStatus.Confirmed)).ToString(), string.Empty);                    
        //            docs.AddRange(docsystem.Documents.GetDocuments(mapping.Value.CabinetID, mapping.Value.DocTypeID, searchParams));

        //            foreach (Document doc in docs)
        //            {
        //                if (!requests.ContainsKey(doc.GUID))
        //                {
        //                    string cnpj = doc.IndexFields["FD_" + mapping.Value.DestCNPJIndexId].ToString();
        //                    if (config.NFConfigurations.ContainsKey(cnpj))
        //                    {
        //                        requests.Add(doc.GUID, new PendingDownload(doc.GUID,
        //                            doc.IndexFields["FD_" + mapping.Value.NFeKeyIndexId].ToString(),
        //                            (ManifestStatus)(Convert.ToInt32(doc.IndexFields["FD_" + mapping.Value.ManifestStatusIndexId])),
        //                            config.NFConfigurations[cnpj]));
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    return requests.Values.ToArray();
        //}

        /// <summary>
        /// Tenta baixar NFe. Caso ainda não tenha sido feito o manifesto, não será possível baixar, e será retornado null.
        /// </summary>
        /// <param name="key">Chave da NFe</param>
        /// <param name="createdOn">Data de emissão da nota (apenas para log)</param>
        /// <param name="stateNumber">Código da UF</param>
        /// <param name="cnpj">CNPJ</param>
        /// <param name="certificate">Certificado para chamada ao WS da SEFAZ</param>
        /// <param name="log">Mecanismo de log</param>
        /// <param name="discard">Se retornar como true, essa nota deve ser descartada (não cadastrar no DS)</param>
        /// <param name="discardReason">Motivo para descarte (preenchido apenas se discard == true)</param>
        /// <returns>XmlDocument</returns>
        internal XmlDocument DownloadNFe(string key, DateTime createdOn, string stateNumber, string cnpj, X509Certificate2 certificate,
            object log, out bool discard, out string discardReason)
        {
            discard = false;
            discardReason = string.Empty;

            try
            {
                NFeDistribuicaoDFeSoapClient client = new NFeDistribuicaoDFeSoapClient();
                client.ClientCredentials.ClientCertificate.Certificate = certificate;

                // Pegando XML modelo para envio
                XmlDocument template = new XmlDocument();
                using (StreamReader reader = new StreamReader(this.GetType().Assembly.GetManifestResourceStream("DS.NFIntegration.SEFAZ.NFeDistribuicaoDFe.Template_Download.xml")))
                {
                    template.LoadXml(reader.ReadToEnd());
                }

                XmlNamespaceManager ns = new XmlNamespaceManager(template.NameTable);
                ns.AddNamespace("nfens", "http://www.portalfiscal.inf.br/nfe");

                template.DocumentElement.SelectSingleNode("nfens:cUFAutor", ns).InnerText = stateNumber;
                template.DocumentElement.SelectSingleNode("nfens:CNPJ", ns).InnerText = cnpj;
                template.DocumentElement.SelectSingleNode("nfens:consChNFe/nfens:chNFe", ns).InnerText = key;

                XmlDocument returnDocument = null;
                Debug debug = new Debug();

                try
                {
                    XmlElement returnElement = client.nfeDistDFeInteresse(template.DocumentElement);

                    if (returnElement == null)
                        return null;

                    returnDocument = new XmlDocument();
                    returnDocument.LoadXml(returnElement.OuterXml);

                    debug.Log(returnDocument, "Nota");
                }
                catch (Exception ex)
                {
                    //log.Write(string.Format("DownloadNFe - Erro obtendo nota {0}", key), ex, true);
                    return null;
                }

                ns = new XmlNamespaceManager(returnDocument.NameTable);
                ns.AddNamespace("nfens", "http://www.portalfiscal.inf.br/nfe");

                XmlNodeList docs = returnDocument.DocumentElement.SelectNodes("nfens:loteDistDFeInt/nfens:docZip", ns);
                string status = returnDocument.DocumentElement.SelectSingleNode("//nfens:cStat", ns).InnerText;

                if (status == "632")
                {
                    // Fora do prazo para baixar a nota.
                    //if (createdOn == DateTime.MinValue)
                    //    log.Write(string.Format("DownloadNFe - Nota {0} do CNPJ {1} fora do prazo para download.", key, cnpj), true);
                    //else
                    //    log.Write(string.Format("DownloadNFe - Nota {0} com emissão em {1} do CNPJ {2} fora do prazo para download.",
                    //        key, createdOn.ToString("dd/MM/yyyy"), cnpj), true);

                    discard = true;
                    discardReason = "Fora do prazo para download";

                    return null;
                }
                else if (status == "633")
                {
                    // Necessário manifesto.
                    discard = false;
                    return null;
                }
                else if (status == "138" && docs != null && docs.Count > 0)
                {
                    // Download disponibilizado.
                    discard = false;

                    foreach (XmlNode doc in docs)
                    {
                        // Descompactando e guardando o XML parcial da NFe (o XML completo vem depois do manifesto).
                        XmlDocument nfeDoc = new NFeCommon(null).UncompressDocument(doc.InnerText);

                        // Verificando se é nota ou algum outro evento.
                        if (doc.Attributes["schema"].InnerText.StartsWith("resNFe") || doc.Attributes["schema"].InnerText.StartsWith("procNFe"))
                        {
                            debug.Log(nfeDoc, "Nota");

                            XmlNamespaceManager ns2 = new XmlNamespaceManager(nfeDoc.NameTable);
                            ns2.AddNamespace("nfens", "http://www.portalfiscal.inf.br/nfe");

                            if (doc.Attributes["schema"].InnerText.StartsWith("resNFe"))
                            {
                                // Status diferente de 1 indica acesso negado (2) ou nota cancelada (3)
                                if (nfeDoc.DocumentElement.SelectSingleNode("nfens:cSitNFe", ns2).InnerText == "1")
                                {
                                    // Verificando se veio completo, ou apenas o resumo.
                                    if (nfeDoc.DocumentElement.SelectSingleNode("//nfens:infNFe/@Id", ns2) == null)
                                        return null;

                                    return nfeDoc;
                                }
                            }
                            else
                            {
                                if (nfeDoc.DocumentElement.SelectSingleNode("//nfens:infNFe/@Id", ns2) == null)
                                    return null;

                                return nfeDoc;
                            }
                        }
                    }

                    // Desconsiderar nota.
                    discard = true;
                    discardReason = "Acesso negado ou nota cancelada";

                    return null;
                }
                else
                {
                    // Não retornou, então vou logar o motivo e sigo em frente.
                    XmlNode reason = returnDocument.DocumentElement.SelectSingleNode("//nfens:xMotivo", ns);
                    //if (reason == null)
                    //    log.Write(string.Format("DownloadNFe - Nota {0} não retornada para CNPJ {1}", key, cnpj), true);
                    //else
                    //    log.Write(string.Format("DownloadNFe - Nota {0} não retornada para CNPJ {1}: {2}", key, cnpj, reason.InnerText), true);

                    discard = true;
                    discardReason = "Nota não retornada";

                    return null;
                }
            }
            catch (Exception ex)
            {
                //log.Write(string.Format("Retrieve - Erro baixando nota para CNPJ {0}", cnpj), ex, true);
                return null;
            }
        }
    }
}
