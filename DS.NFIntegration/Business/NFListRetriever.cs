﻿using DS.NFIntegration.Entities;
using DS.NFIntegration.Exceptions;
using DS.NFIntegration.SEFAZ.NFeDistribuicaoDFe;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel.Channels;
using System.Text;
using System.Xml;

namespace DS.NFIntegration.Business
{
    /// <summary>
    /// Esta classe é responsável por buscar, baixar e armazenar novas notas.
    /// </summary>
    public class NFListRetriever
    {
        /// <summary>
        /// Carrega as notas disponíveis desde o último processamento.
        /// </summary>
        public void Retrieve()
        {
            string logPath = Path.Combine(Directory.GetCurrentDirectory(), "Log");
            if (!Directory.Exists(logPath))
                Directory.CreateDirectory(logPath);

            //Log log = null;

            try
            {
                //log = new Log(logPath);
                //log.Write("Retrieve - Processamento iniciado...", true);

                // Criar objeto de configuração e verificar permissão para execução das atividades.
                //Configuration config = null;

                //try
                //{
                //    config = new Configuration();

                //    if (config.DSConfiguration.LicenceUsage.ValidFrom > DateTime.Now.Date)
                //    {
                //        log.Write("Utilização do sistema fora da data permitida.", true);
                //        return;
                //    }
                //    else if (config.DSConfiguration.LicenceUsage.ValidUntil < DateTime.Now.Date)
                //    {
                //        log.Write("Utilização do sistema fora da data permitida.", true);
                //        return;
                //    }
                //}
                //catch (Exception ex)
                //{
                //    log.Write("Retrieve - Erro acessando configurações", ex, true);
                //    return;
                //}

                using (NFeCommon common = new NFeCommon(null))
                {
                    foreach (string cnpj in new string[] { })//config.NFConfigurations.Keys)
                    {
                        // Para cada CPNJ:
                        //      - Verificar último código retornado
                        //      - Baixar novas notas
                        //      - Para notas manifestadas
                        //          - Fazer mapeamento XML X Índices
                        //          - Gravar dados e XML no DocSystem
                        //          - Converter XML para DANFE (PDF)
                        //          - Gravar no DocSystem
                        //      - Para notas não manifestadas
                        //          - Fazer mapeamento das informações disponíveis
                        //          - Gravar dados no DocSystem (nenhum arquivo será gravado)

                        X509Certificate2 certificate = null;

                        try
                        {
                            certificate = null;// config.GetCertificate(cnpj);
                            //if (certificate == null)
                            //{
                            //    log.Write(string.Format("Retrieve - Nenhum certificado retornado para CNPJ {0}", cnpj), true);
                            //    continue;
                            //}
                        }
                        catch (Exception ex)
                        {
                            //log.Write(string.Format("Retrieve - Erro obtendo certificado para CNPJ {0}", cnpj), ex, true);
                            continue;
                        }


                        XmlDocument[] nfePartialDocs = null;// GetNextDocuments(config, cnpj, certificate, log);
                        //log.Write(string.Format("Retrieve - Retornada(s) {0} nota(s) para o CNPJ {1}.", nfePartialDocs.Length, cnpj), true);

                        foreach (XmlDocument nfeDoc in nfePartialDocs)
                        {
                            XmlNamespaceManager ns = new XmlNamespaceManager(nfeDoc.NameTable);
                            ns.AddNamespace("nfens", "http://www.portalfiscal.inf.br/nfe");

                            XmlDocument nfeCompleteDoc = null;

                            if (nfeDoc.DocumentElement.SelectSingleNode("//nfens:infNFe/@Id", ns) == null)
                            {
                                // Veio a nota parcial
                                bool discard = false;
                                string discardReason = string.Empty;
                                DateTime createdOn = DateTime.ParseExact(nfeDoc.DocumentElement.SelectSingleNode("nfens:dhEmi", ns).InnerText.Substring(0, 10), "yyyy-MM-dd", null);

                                //nfeCompleteDoc = new NFDownloader().DownloadNFe(nfeDoc.DocumentElement.SelectSingleNode("nfens:chNFe", ns).InnerText,
                                //    createdOn, config.NFConfigurations[cnpj].StateNumber.ToString(), config.NFConfigurations[cnpj].CNPJ, certificate, log, out discard, out discardReason);

                                // Indica se essa nota não deve ser considerada
                                if (discard)
                                    continue;
                            }
                            else
                            {
                                // Veio a nota completa
                                nfeCompleteDoc = nfeDoc;
                            }

                            if (nfeCompleteDoc == null)
                            {
                                try
                                {
                                    Mapping mapping = null;// config.Mappings[config.NFConfigurations[cnpj].CabinetID];

                                    // Ainda deve ser feito o manifesto
                                    //Document document = GetPartialDocument(nfeDoc, mapping, cnpj);

                                    // Verificando se tem manifesto automático
                                    //if (config.NFConfigurations[cnpj].AutomaticManifest)
                                    //    document.IndexFields["FD_" + mapping.ManifestStatusIndexId] = (Convert.ToInt32(ManifestStatus.Confirmed)).ToString();

                                    SavePartialNFe(null, nfeDoc, null);

                                    // Verificando se tem manifesto automático
                                    //if (config.NFConfigurations[cnpj].AutomaticManifest)
                                    //{
                                    //    // Gravo arquivo para ser executado por outro componente
                                    //    using (StreamWriter writer = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), string.Format("PendingManifest_{0}.txt", document.GUID.ToString())), true, Encoding.UTF8))
                                    //    {
                                    //        writer.WriteLine(string.Format("{0};{1};{2};{3}", document.GUID.ToString(), cnpj,
                                    //            nfeDoc.DocumentElement.SelectSingleNode("nfens:chNFe", ns).InnerText,
                                    //            (Convert.ToInt32(ManifestStatus.Confirmed)).ToString()));
                                    //    }
                                    //}
                                }
                                catch (Exception ex)
                                {
                                    //log.Write("Retrieve - Erro montando e salvando nota parcial no DocSystem", ex, true);
                                }
                            }
                            else
                            {
                                try
                                {
                                    // Veio a nota completa (confirmado ou dado ciência por outros meios - não tenho como saber qual)
                                    //Document document = common.GetCompleteDocument(nfeCompleteDoc, cnpj, ManifestStatus.Known, string.Empty);
                                    common.SaveCompleteNFe(null, nfeCompleteDoc, cnpj, Directory.GetCurrentDirectory());
                                }
                                catch (UpdateDocumentException ex)
                                {
                                    //log.Write("Retrieve - Erro montando e salvando nota completa no DocSystem", true);

                                    //foreach (KeyValuePair<string, object> updateDocEntry in ex.Document.IndexFields)
                                    //    log.Write(string.Format("\t{0}: {1}", updateDocEntry.Key, (updateDocEntry.Value == null ? string.Empty : updateDocEntry.Value.ToString())), true);

                                    //log.Write(ex.Message, true);
                                }
                                catch (Exception ex)
                                {
                                    //log.Write("Retrieve - Erro montando e salvando nota completa no DocSystem", ex, true);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //if (log != null)
                //    log.Write("Retrieve - Erro não tratado", ex, true);
            }
            finally
            {
                //if (log != null)
                //{
                //    log.Write("Retrieve - Processamento finalizado.", true);
                //    log.Dispose();
                //    log = null;
                //}
            }
        }

        /// <summary>
        /// Este método pega as notas que ainda não foram baixadas, com base no último NSU
        /// </summary>
        /// <param name="config">Configuração</param>
        /// <param name="cnpj">CNPJ</param>
        /// <param name="certificate">Certificado para chamada ao WS da SEFAZ</param>
        /// <param name="log">Mecanismo de log</param>
        /// <returns>XmlDocument[]</returns>
        private XmlDocument[] GetNextDocuments(object config, string cnpj, X509Certificate2 certificate, object log)
        {
            NFeDistribuicaoDFeSoapClient client = new NFeDistribuicaoDFeSoapClient();
            client.ClientCredentials.ClientCertificate.Certificate = certificate;

            // Pegando XML modelo para envio
            XmlDocument template = new XmlDocument();
            using (StreamReader reader = new StreamReader(this.GetType().Assembly.GetManifestResourceStream("DS.NFIntegration.SEFAZ.NFeDistribuicaoDFe.Template.xml")))
            {
                template.LoadXml(reader.ReadToEnd());
            }

            XmlNamespaceManager ns = new XmlNamespaceManager(template.NameTable);
            ns.AddNamespace("nfens", "http://www.portalfiscal.inf.br/nfe");

            //template.DocumentElement.SelectSingleNode("nfens:cUFAutor", ns).InnerText = config.NFConfigurations[cnpj].StateNumber.ToString();
            //template.DocumentElement.SelectSingleNode("nfens:CNPJ", ns).InnerText = cnpj;
            //template.DocumentElement.SelectSingleNode("nfens:distNSU/nfens:ultNSU", ns).InnerText = config.NFConfigurations[cnpj].LastNSU.PadLeft(15, Convert.ToChar("0"));

            XmlDocument returnDocument = null;
            Debug debug = new Debug();

            try
            {
                XmlElement returnElement = client.nfeDistDFeInteresse(template.DocumentElement);

                if (returnElement == null)
                    return new XmlDocument[] { };

                returnDocument = new XmlDocument();
                returnDocument.LoadXml(returnElement.OuterXml);

                debug.Log(returnDocument, "Pendentes");
            }
            catch (Exception ex)
            {
                //log.Write(string.Format("Retrieve - Erro obtendo lista de notas para CNPJ {0}", cnpj), ex, true);
                return new XmlDocument[] { };
            }

            ns = new XmlNamespaceManager(returnDocument.NameTable);
            ns.AddNamespace("nfens", "http://www.portalfiscal.inf.br/nfe");

            XmlNodeList docs = returnDocument.DocumentElement.SelectNodes("nfens:loteDistDFeInt/nfens:docZip", ns);
            if (docs == null || docs.Count == 0)
            {
                XmlNode reason = returnDocument.DocumentElement.SelectSingleNode("nfens:xMotivo", ns);
                if (reason != null)
                {
                    //log.Write(string.Format("Retrieve - Nenhuma nota retornada para CNPJ {0}: {1}", cnpj, reason.InnerText), true);
                    return new XmlDocument[] { };
                }
            }

            Dictionary<string, XmlDocument> nfes = new Dictionary<string, XmlDocument>(docs.Count);

            foreach (XmlNode doc in docs)
            {
                // Guardando o último NSU
                //config.NFConfigurations[cnpj].LastNSU = doc.Attributes["NSU"].InnerText;

                // Descompactando e guardando o XML parcial da NFe (o XML completo vem depois do manifesto)
                XmlDocument partialNFe = new NFeCommon(null).UncompressDocument(doc.InnerText);

                // Verificando se é nota ou algum outro evento
                if (!doc.Attributes["schema"].InnerText.StartsWith("resNFe") && !doc.Attributes["schema"].InnerText.StartsWith("procNFe"))
                {
                    debug.Log(partialNFe, "Parcial_OutroSchema");
                }
                else
                {
                    debug.Log(partialNFe, "Parcial");

                    XmlNamespaceManager ns2 = new XmlNamespaceManager(partialNFe.NameTable);
                    ns2.AddNamespace("nfens", "http://www.portalfiscal.inf.br/nfe");

                    // Status diferente de 1 indica acesso negado (2) ou nota cancelada (3)
                    if ((doc.Attributes["schema"].InnerText.StartsWith("resNFe") && partialNFe.DocumentElement.SelectSingleNode("nfens:cSitNFe", ns2).InnerText == "1")
                        || doc.Attributes["schema"].InnerText.StartsWith("procNFe"))
                    {
                        // Chave da nota
                        string key = partialNFe.DocumentElement.SelectSingleNode("//nfens:chNFe", ns).InnerText;

                        // Se não está na lista, adicionou.
                        // Se já está eu verifico
                        if (!nfes.ContainsKey(key))
                            nfes.Add(key, partialNFe);
                        else if (doc.Attributes["schema"].InnerText.StartsWith("procNFe"))
                            nfes[key] = partialNFe;
                    }
                }
            }

            // Salvo para persistir o número referente ao último NSU.
            //config.Save(false);

            return nfes.Values.ToArray();
        }

        #region NFe Parcial

        /// <summary>
        /// Retorna um documento (índices) do DocSystem com as informações da NFe
        /// </summary>
        /// <param name="nfeDoc">Nota completa</param>
        /// <param name="config">Configuração</param>
        /// <param name="cnpj">CNPJ</param>
        /// <returns>Document</returns>
        //private Document GetPartialDocument(XmlDocument nfeDoc, Mapping mapping, string cnpj)
        //{
        //    XmlNamespaceManager ns = new XmlNamespaceManager(nfeDoc.NameTable);
        //    ns.AddNamespace("nfens", "http://www.portalfiscal.inf.br/nfe");

        //    Dictionary<string, object> indexFields = new Dictionary<string, object>();

        //    indexFields.Add(mapping.DestCNPJIndexId, cnpj);
        //    indexFields.Add(mapping.NFeKeyIndexId, nfeDoc.DocumentElement.SelectSingleNode("nfens:chNFe", ns).InnerText);
        //    indexFields.Add(mapping.ManifestStatusIndexId, (Convert.ToInt32(ManifestStatus.Pending)).ToString());

        //    // O mapeamento é diferente no XML de manifesto, então eu verifico se há elementos em comum.
        //    foreach (MappingItem item in mapping.Items)
        //    {
        //        if (item.IsFixedValue)
        //        {
        //            indexFields.Add(item.DSIndexID, item.NFPath);
        //        }
        //        else
        //        {
        //            switch (item.NFPath)
        //            {
        //                case "//nfens:infNFe/nfens:emit/nfens:CNPJ":
        //                    indexFields.Add(item.DSIndexID, nfeDoc.DocumentElement.SelectSingleNode("nfens:CNPJ", ns).InnerText);
        //                    break;

        //                case "//nfens:infNFe/nfens:emit/nfens:xNome":
        //                    indexFields.Add(item.DSIndexID, nfeDoc.DocumentElement.SelectSingleNode("nfens:xNome", ns).InnerText);
        //                    break;

        //                case "//nfens:infNFe/nfens:emit/nfens:IE":
        //                    indexFields.Add(item.DSIndexID, nfeDoc.DocumentElement.SelectSingleNode("nfens:IE", ns).InnerText);
        //                    break;

        //                case "//nfens:infNFe/nfens:ide/nfens:dhEmi":
        //                    string issueDateValue = nfeDoc.DocumentElement.SelectSingleNode("nfens:dhEmi", ns).InnerText;
        //                    DateTime issueDate = DateTime.ParseExact(issueDateValue.Substring(0, 10), "yyyy-MM-dd", null);
        //                    indexFields.Add(item.DSIndexID, issueDate);
        //                    break;

        //                case "//nfens:infNFe/nfens:ide/nfens:tpNF":
        //                    indexFields.Add(item.DSIndexID, nfeDoc.DocumentElement.SelectSingleNode("nfens:tpNF", ns).InnerText);
        //                    break;

        //                case "//nfens:infNFe/nfens:total/nfens:ICMSTot/nfens:vNF":
        //                    indexFields.Add(item.DSIndexID, Convert.ToDouble(nfeDoc.DocumentElement.SelectSingleNode("nfens:vNF", ns).InnerText, new CultureInfo("en-US")));
        //                    break;
        //            }
        //        }
        //    }

        //    return Document.CreateNew(mapping.CabinetID, mapping.DocTypeID, indexFields);
        //}

        /// <summary>
        /// Salva NFe parcial no DocSystem
        /// </summary>
        /// <param name="doc">Documento contendo os índices</param>
        /// <param name="nfeDoc">XML parcial da NFe</param>
        /// <param name="config">Configuração</param>
        /// <param name="cnpj">CNPJ</param>
        private void SavePartialNFe(object doc, XmlDocument nfeDoc, object config)
        {
            //using (DocSystem docsystem = new DocSystem(config.DSConfiguration.Domain, config.DSConfiguration.UserName,
            //    config.DSConfiguration.OriginalPassword, string.Empty, string.Empty, string.Empty, string.Empty, config.DSConfiguration.BaseUrl))
            //{
            //    DS.Common.Entities.File nfeFile = new NFeCommon(null).GetFile(System.Text.Encoding.UTF8.GetBytes(nfeDoc.OuterXml), "NFe_Parcial.xml", 1, Directory.GetCurrentDirectory());
            //    docsystem.Documents.CreateNewDocument(doc, new Common.Entities.File[] { nfeFile });
            //}
        }

        #endregion
    }
}
