﻿using DS.NFIntegration.Entities;
using DS.NFIntegration.SEFAZ.RecepcaoEvento;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Xml;

namespace DS.NFIntegration.Business
{
    public class Manifest
    {
        public void DoPendingManifest()
        {
            string logPath = Path.Combine(Directory.GetCurrentDirectory(), "Log");
            if (!Directory.Exists(logPath))
                Directory.CreateDirectory(logPath);

            string[] files = Directory.GetFiles(Directory.GetCurrentDirectory());

            foreach (string file in files)
            {
                string fileName = Path.GetFileName(file);
                if (!fileName.ToLower().StartsWith("pendingmanifest_"))
                    continue;

                string docGuid = string.Empty;
                string cnpj = string.Empty;
                string nfeKey = string.Empty;
                string statusText = string.Empty;

                using (StreamReader reader = new StreamReader(file))
                {
                    string[] lineParts = reader.ReadLine().Split(new char[] { Convert.ToChar(";") });
                    if (lineParts.Length < 4)
                        continue;

                    docGuid = lineParts[0];
                    cnpj = lineParts[1];
                    nfeKey = lineParts[2];
                    statusText = lineParts[3];
                }

                //if (!config.NFConfigurations.ContainsKey(cnpj))
                //{
                //    log.Write(string.Format("DoPendingManifest - CNPJ {0} não está configurado.", cnpj), true);
                //    continue;
                //}
                //else if (!config.Mappings.ContainsKey(config.NFConfigurations[cnpj].CabinetID))
                //{
                //    log.Write(string.Format("DoPendingManifest - CNPJ {0} não está mapeado.", cnpj), true);
                //    continue;
                //}

                //Mapping mapping = config.Mappings[config.NFConfigurations[cnpj].CabinetID];

                DataTable dt = new DataTable();
                dt.Columns.Add("GUID", typeof(string));
                //dt.Columns.Add("FD_" + mapping.NFeKeyIndexId, typeof(string));
                //dt.Columns.Add("FD_" + mapping.ManifestStatusIndexId, typeof(string));

                DataRow docRow = dt.NewRow();

                docRow["GUID"] = docGuid;
                //docRow["FD_" + mapping.NFeKeyIndexId] = nfeKey;
                //docRow["FD_" + mapping.ManifestStatusIndexId] = statusText;

                dt.Rows.Add(docRow);

                string returnValue = ManifestNFe(docRow, Directory.GetCurrentDirectory(), cnpj);

                File.Delete(file);

                //if (returnValue.ToLower().IndexOf("sucesso") >= 0)
                //    log.Write(string.Format("DoPendingManifest - CNPJ {0}: chave {1} manifestada com sucesso", cnpj, nfeKey), true);
                //else
                //    log.Write(string.Format("DoPendingManifest - CNPJ {0}: chave {1} - {2}", cnpj, nfeKey, returnValue), true);
            }
        }

        public string ManifestNFe(DataRow docRow, string baseDirectory, string cnpj)
        {
            if (docRow == null)
                return "Simulação (docRow = null)";

            string nfeKey = string.Empty;
            string statusText = string.Empty;

            try
            {
                //Configuration config = new Configuration(baseDirectory);

                //if (!config.DSConfiguration.LicenceUsage.Manifest)
                //    return "Manifesto não é permitido para esta licença.";
                //else if (config.DSConfiguration.LicenceUsage.ValidFrom > DateTime.Now.Date)
                //    return "Utilização do sistema fora da data permitida.";
                //else if (config.DSConfiguration.LicenceUsage.ValidUntil < DateTime.Now.Date)
                //    return "Utilização do sistema fora da data permitida.";

                //// Primeiro passo é pegar a configuração de mapeamento.
                //Mapping mapping = config.Mappings[config.NFConfigurations[cnpj].CabinetID];

                // Com a configuração pego os dados da nota necessários para a chamada.
                //nfeKey = docRow["FD_" + mapping.NFeKeyIndexId].ToString();
                string stateNumber = string.Empty;// config.NFConfigurations[cnpj].StateNumber.ToString();
                //statusText = docRow["FD_" + mapping.ManifestStatusIndexId].ToString();
                ManifestStatus status = (ManifestStatus)(Convert.ToInt32(statusText));

                if (status == ManifestStatus.Pending)
                    return "Nenhuma mudança solicitada.";

                // Pegando código e descrição do evento.
                XmlDocument requestData = GetRequestData(cnpj, nfeKey, stateNumber, status, baseDirectory);

                // Pegando o certificado.
                X509Certificate2 certificate = null;

                try
                {
                    //certificate = config.GetCertificate(cnpj);
                    if (certificate == null)
                    {
                        LogMessage(string.Format("ManifestNFe - Nenhum certificado retornado para o CNPJ {0}", cnpj), baseDirectory, true);
                        return "Certificado não encontrado.";
                    }
                }
                catch (Exception ex)
                {
                    LogMessage(string.Format("ManifestNFe - Erro obtendo certificado para o CNPJ {0}: {1}", cnpj, ex.Message), baseDirectory, true);
                    return "Erro obtendo certificado - verificar log.";
                }

                // Fazendo chamada.
                XmlDocument returnData = null;// DoManifest(stateNumber, requestData, certificate);

                // Verificando status
                XmlNamespaceManager ns = new XmlNamespaceManager(returnData.NameTable);
                ns.AddNamespace("nfens", "http://www.portalfiscal.inf.br/nfe");

                if (returnData.DocumentElement.SelectSingleNode("nfens:retEvento/nfens:infEvento/nfens:cStat", ns) == null)
                {
                    string reason = returnData.DocumentElement.SelectSingleNode("nfens:xMotivo", ns).InnerText;

                    string message = string.Format("ManifestNFe ({0}) - {1}", docRow["GUID"].ToString(), reason);
                    LogMessage(message, baseDirectory, true);

                    return reason;
                }
                else
                {
                    string responseStatus = returnData.DocumentElement.SelectSingleNode("nfens:retEvento/nfens:infEvento/nfens:cStat", ns).InnerText;

                    if (responseStatus == "135" || responseStatus == "136")
                    {
                        return "Manifesto executado com sucesso.";
                    }
                    else
                    {
                        string reason = returnData.DocumentElement.SelectSingleNode("nfens:retEvento/nfens:infEvento/nfens:xMotivo", ns).InnerText;

                        string message = string.Format("ManifestNFe ({0}) - {1}", docRow["GUID"].ToString(), reason);
                        LogMessage(message, baseDirectory, true);

                        return reason;
                    }
                }
            }
            catch (Exception ex)
            {
                try
                {
                    string message = string.Format("ManifestNFe ({0}) - {1}: {2} - {3}", docRow["GUID"].ToString(), DateTime.Now.ToString(), ex.Message, ex.StackTrace);
                    LogMessage(message, baseDirectory, false);
                }
                catch (Exception ex2)
                {
                    return string.Format("Erro: {0} / erro gravando log: {1}", ex.Message, ex2.Message);
                }

                try
                {
                    if (!string.IsNullOrEmpty(nfeKey) && !string.IsNullOrEmpty(statusText))
                    {
                        using (StreamWriter writer = new StreamWriter(Path.Combine(baseDirectory, string.Format("PendingManifest_{0}.txt", docRow["GUID"].ToString())), true, Encoding.UTF8))
                        {
                            writer.WriteLine(string.Format("{0};{1};{2};{3}", docRow["GUID"].ToString(), cnpj, nfeKey, statusText));
                        }

                        return "Reprocessamento";
                    }
                    else
                    {
                        return "Erro - verificar log.";
                    }
                }
                catch (Exception ex2)
                {
                    return string.Format("Erro: {0} / erro gravando arquivo de pendência: {1}", ex.Message, ex2.Message);
                }
            }
        }

        private XmlDocument GetRequestData(string cnpj, string nfeKey, string stateNumber, ManifestStatus status, string baseDirectory)
        {
            string eventCode = string.Empty;
            string eventDesc = string.Empty;

            GetEventRequestData(status, out eventCode, out eventDesc);

            // Pegando XML modelo para envio
            XmlDocument template = new XmlDocument();
            using (StreamReader reader = new StreamReader(this.GetType().Assembly.GetManifestResourceStream("DS.NFIntegration.SEFAZ.Recepcaoevento.Template.xml")))
            {
                template.LoadXml(reader.ReadToEnd());
            }

            XmlNamespaceManager ns = new XmlNamespaceManager(template.NameTable);
            ns.AddNamespace("nfens", "http://www.portalfiscal.inf.br/nfe");

            template.DocumentElement.SelectSingleNode("nfens:evento/nfens:infEvento", ns).Attributes["Id"].InnerText = string.Format("ID{0}{1}01", eventCode, nfeKey);
            template.DocumentElement.SelectSingleNode("nfens:evento/nfens:infEvento/nfens:cOrgao", ns).InnerText = "91";// stateNumber;
            template.DocumentElement.SelectSingleNode("nfens:evento/nfens:infEvento/nfens:CNPJ", ns).InnerText = cnpj;
            template.DocumentElement.SelectSingleNode("nfens:evento/nfens:infEvento/nfens:chNFe", ns).InnerText = nfeKey;
            template.DocumentElement.SelectSingleNode("nfens:evento/nfens:infEvento/nfens:tpEvento", ns).InnerText = eventCode;
            template.DocumentElement.SelectSingleNode("nfens:evento/nfens:infEvento/nfens:detEvento/nfens:descEvento", ns).InnerText = eventDesc;

            int timeZoneHours = Convert.ToInt32(DateTime.Now.Subtract(DateTime.UtcNow).TotalHours);
            string timeZoneRepresentation = string.Empty;

            if (timeZoneHours >= 0)
                timeZoneRepresentation = string.Format("+{0}:00", timeZoneHours.ToString().PadLeft(2, Convert.ToChar("0")));
            else
                timeZoneRepresentation = string.Format("-{0}:00", (timeZoneHours * -1).ToString().PadLeft(2, Convert.ToChar("0")));

            template.DocumentElement.SelectSingleNode("nfens:evento/nfens:infEvento/nfens:dhEvento", ns).InnerText = string.Format("{0}{1}",
                DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"), timeZoneRepresentation);

            return template;
        }

        private void GetEventRequestData(ManifestStatus status, out string eventCode, out string eventDesc)
        {
            eventCode = string.Empty;
            eventDesc = string.Empty;

            switch (status)
            {
                case ManifestStatus.Confirmed:
                    eventCode = "210200";
                    eventDesc = "Confirmacao da Operacao";
                    break;

                case ManifestStatus.Known:
                    eventCode = "210210";
                    eventDesc = "Ciencia da Operacao";
                    break;

                case ManifestStatus.Refused:
                    eventCode = "210240";
                    eventDesc = "Operacao nao Realizada";
                    break;

                case ManifestStatus.Unknown:
                    eventCode = "210220";
                    eventDesc = "Desconhecimento da Operacao";
                    break;
            }
        }

        private XmlDocument DoManifest(string stateNumber, XmlDocument data, X509Certificate2 certificate)
        {
            NFeCustomBinding nfeCustomBinding = new NFeCustomBinding();
            RecepcaoEventoSoap12Client client = new RecepcaoEventoSoap12Client(
                nfeCustomBinding.GetNFeCustomBinding(),
                new System.ServiceModel.EndpointAddress("https://www.nfe.fazenda.gov.br/RecepcaoEvento/RecepcaoEvento.asmx"));

            client.ClientCredentials.ClientCertificate.Certificate = certificate;

            XmlNamespaceManager ns = new XmlNamespaceManager(data.NameTable);
            ns.AddNamespace("nfens", "http://www.portalfiscal.inf.br/nfe");

            // Assinatura /////////////////////////////////////////////////////////////////////
            string id = data.DocumentElement.SelectSingleNode("nfens:evento/nfens:infEvento", ns).Attributes["Id"].InnerText;
            SignedXml signedXml = new SignedXml(data);
            signedXml.SigningKey = certificate.PrivateKey;

            Reference reference = new Reference("#" + id);
            reference.AddTransform(new XmlDsigEnvelopedSignatureTransform());
            reference.AddTransform(new XmlDsigC14NTransform());
            signedXml.AddReference(reference);

            KeyInfo keyInfo = new KeyInfo();
            keyInfo.AddClause(new KeyInfoX509Data(certificate));

            signedXml.KeyInfo = keyInfo;

            signedXml.ComputeSignature();

            XmlElement xmlSignature = data.CreateElement("Signature", "http://www.w3.org/2000/09/xmldsig#");
            XmlElement xmlSignedInfo = signedXml.SignedInfo.GetXml();
            XmlElement xmlKeyInfo = signedXml.KeyInfo.GetXml();

            XmlElement xmlSignatureValue = data.CreateElement("SignatureValue", xmlSignature.NamespaceURI);
            string signBase64 = Convert.ToBase64String(signedXml.Signature.SignatureValue);
            XmlText text = data.CreateTextNode(signBase64);
            xmlSignatureValue.AppendChild(text);

            xmlSignature.AppendChild(data.ImportNode(xmlSignedInfo, true));
            xmlSignature.AppendChild(xmlSignatureValue);
            xmlSignature.AppendChild(data.ImportNode(xmlKeyInfo, true));

            data.DocumentElement.ChildNodes[1].AppendChild(xmlSignature);
            //data.DocumentElement.SelectSingleNode("nfens:evento", ns).AppendChild(xmlSignature);
            ///////////////////////////////////////////////////////////////////////////////////

            nfeCabecMsg cabec = new nfeCabecMsg();
            cabec.cUF = stateNumber;
            cabec.versaoDados = "1.00";

            XmlNode returnNode = client.nfeRecepcaoEvento(ref cabec, (XmlNode)(data.DocumentElement));

            XmlDocument returnDoc = new XmlDocument();
            returnDoc.LoadXml(returnNode.OuterXml);

            return returnDoc;
        }

        private void LogMessage(string message, string baseDirectory, bool trapException)
        {
            try
            {
                string logPath = Path.Combine(baseDirectory, "Log");
                if (!Directory.Exists(logPath))
                    Directory.CreateDirectory(logPath);

                //using (Log log = new Log(logPath))
                //{
                //    log.Write(message, true);
                //}
            }
            catch (Exception ex)
            {
                if (!trapException)
                    throw ex;
            }
        }
    }
}
