﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text;

namespace DS.NFIntegration.Business
{
    internal class NFeCustomBinding
    {
        public CustomBinding GetNFeCustomBinding()
        {
            TextMessageEncodingBindingElement text = new TextMessageEncodingBindingElement();
            text.MessageVersion = MessageVersion.Soap12;

            HttpsTransportBindingElement https = new HttpsTransportBindingElement();
            https.AuthenticationScheme = System.Net.AuthenticationSchemes.Digest;
            https.RequireClientCertificate = true;

            CustomBinding customBinding = new CustomBinding();
            customBinding.Name = "NFe";
            customBinding.Elements.Add(text);
            customBinding.Elements.Add(https);

            return customBinding;
        }
    }
}
