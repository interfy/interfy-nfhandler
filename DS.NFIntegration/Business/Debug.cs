﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Xml;

namespace DS.NFIntegration.Business
{
    internal class Debug
    {
        public void Log(XmlDocument xml, string name)
        {
            if (ConfigurationManager.AppSettings["Debug"] != "true")
                return;

            string debugPath = Path.Combine(Directory.GetCurrentDirectory(), "Debug");
            if (!Directory.Exists(debugPath))
                Directory.CreateDirectory(debugPath);

            string fileName = Path.Combine(debugPath, DateTime.Now.Ticks.ToString() + "_" + name + ".xml");
            xml.Save(fileName);
        }
    }
}
