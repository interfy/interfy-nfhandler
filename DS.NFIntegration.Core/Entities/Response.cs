﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DS.NFIntegration.Core.Entities
{
    /// <summary>
    /// Retorno do SEFAZ
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Response<T>
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="status">Status</param>
        /// <param name="reason">Movito</param>
        /// <param name="data">Dados</param>
        public Response(int status, string reason, T data)
        {
            Status = status;
            Reason = reason;
            Data = data;
        }

        /// <summary>
        /// Status
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// Movito
        /// </summary>
        public string Reason { get; set; }

        /// <summary>
        /// Dados
        /// </summary>
        public T Data { get; set; }
    }
}
