﻿namespace DS.NFIntegration.Core.Entities
{
    /// <summary>
    /// Configuração para acesso ao S3
    /// </summary>
    public class S3AccessConfiguration
    {
        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="key">Chave</param>
        /// <param name="secret">Segredo</param>
        /// <param name="region">Região</param>
        /// <param name="bucket">Bucket</param>
        public S3AccessConfiguration(string key, string secret, string region, string bucket)
        {
            Key = key;
            Secret = secret;
            Region = region;

            // Não posso passar para caixa baixa, porque a busca no Interfy é case sensitive.
            bucket = bucket.Replace("s3://", string.Empty).Replace("S3://", string.Empty);

            if (bucket.IndexOf("/") > 0)
            {
                Bucket = bucket.Substring(0, bucket.IndexOf("/"));
                Path = bucket.Substring(bucket.IndexOf("/") + 1);
                if (!Path.EndsWith("/"))
                    Path += "/";
            }
            else
            {
                Bucket = bucket;
            }
        }

        /// <summary>
        /// Chave
        /// </summary>
        public string Key { get; private set; }

        /// <summary>
        /// Segredo
        /// </summary>
        public string Secret { get; private set; }

        /// <summary>
        /// Região
        /// </summary>
        public string Region { get; private set; }

        /// <summary>
        /// Bucket
        /// </summary>
        public string Bucket { get; private set; }

        /// <summary>
        /// Caminho para o arquivo
        /// </summary>
        public string Path { get; private set; }
    }
}
