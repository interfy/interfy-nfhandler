﻿using System;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace DS.NFIntegration.Core.Business
{
    internal class NFeCustomBinding
    {
        public CustomBinding GetNFeCustomBinding()
        {
            TextMessageEncodingBindingElement text = new TextMessageEncodingBindingElement();
            text.MessageVersion = MessageVersion.Soap11;

            HttpsTransportBindingElement https = new HttpsTransportBindingElement();
            https.AuthenticationScheme = System.Net.AuthenticationSchemes.Digest;
            https.RequireClientCertificate = true;
            https.MaxReceivedMessageSize = 1024000;
            https.MaxBufferSize = 1024000;

            CustomBinding customBinding = new CustomBinding();
            customBinding.Name = "NFe";
            customBinding.Elements.Add(text);
            customBinding.Elements.Add(https);

            return customBinding;
        }

        public EndpointAddress GetEndpointAddress()
        {
            return new EndpointAddress("https://www1.nfe.fazenda.gov.br/NFeDistribuicaoDFe/NFeDistribuicaoDFe.asmx");
        }
    }
}
