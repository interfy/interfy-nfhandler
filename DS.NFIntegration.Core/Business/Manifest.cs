﻿using System;
using System.IO;
using System.Security.Cryptography.Xml;
using System.Security.Cryptography.X509Certificates;
using System.Xml;
using DS.NFIntegration.Core.Entities;
using DS.NFIntegration.SEFAZ.RecepcaoEvento;
using System.Collections;
using DS.NFIntegration.Core.Interfaces;

namespace DS.NFIntegration.Core.Business
{
    /// <summary>
    /// Classe para manifesto
    /// </summary>
    public class Manifest
    {
        private string cnpj = string.Empty;
        private X509Certificate2 certificate = null;
        private ILogger logger = null;
        private NFeCommon common = null;
        private string responseUrl = string.Empty;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="cnpj">CNPJ</param>
        /// <param name="certificate">Certificado</param>
        /// <param name="responseUrl">Url de retorno para o Interfy</param>
        /// <param name="logger">Logger do Lambda</param>
        public Manifest(string cnpj, X509Certificate2 certificate, string responseUrl, ILogger logger)
        {
            this.cnpj = cnpj;
            this.certificate = certificate;
            this.responseUrl = responseUrl;
            this.logger = logger;
            this.common = new NFeCommon(this.logger);
        }

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="cnpj">CNPJ</param>
        /// <param name="certificate">Certificado</param>
        /// <param name="responseUrl">Url de retorno para o Interfy</param>
        /// <param name="logger">Logger</param>
        internal Manifest(string cnpj, X509Certificate2 certificate, string responseUrl, LambdaLogger logger)
        {
            this.cnpj = cnpj;
            this.certificate = certificate;
            this.responseUrl = responseUrl;
            this.logger = logger;
            this.common = new NFeCommon(this.logger);
        }

        /// <summary>
        /// Efetua manifesto de uma nota
        /// </summary>
        /// <param name="statusCode">Status desejado</param>
        /// <param name="nfeKey">Chave da nota</param>
        /// <param name="notifyOnSuccess">Indica se é para retornar para o Interfy em caso de sucesso</param>
        /// <returns>Response&lt;bool&gt;</returns>
        public Response<bool> ManifestNFe(string statusCode, string nfeKey, bool notifyOnSuccess)
        {
            ManifestStatus manifestStatus = ManifestStatus.Pending;
            string eventDesc = string.Empty;
            GetEventRequestData(statusCode, out manifestStatus, out eventDesc);

            return ManifestNFe(manifestStatus, nfeKey, notifyOnSuccess);
        }

        /// <summary>
        /// Efetua manifesto de uma nota
        /// </summary>
        /// <param name="status">Status desejado</param>
        /// <param name="nfeKey">Chave da nota</param>
        /// <param name="notifyOnSuccess">Indica se é para retornar para o Interfy em caso de sucesso</param>
        /// <returns>Response&lt;bool&gt;</returns>
        public Response<bool> ManifestNFe(ManifestStatus status, string nfeKey, bool notifyOnSuccess)
        {
            try
            {
                if (status == ManifestStatus.Pending)
                    return new Response<bool>(-1, "Nenhuma mudança solicitada.", false);

                // Pegando código e descrição do evento.
                XmlDocument requestData = GetRequestData(cnpj, nfeKey, status);

                // Fazendo chamada.
                XmlDocument returnData = DoManifest(requestData, certificate);

                // Verificando status
                XmlNamespaceManager ns = new XmlNamespaceManager(returnData.NameTable);
                ns.AddNamespace("nfens", "http://www.portalfiscal.inf.br/nfe");

                // Dados para retorno
                string eventCode = string.Empty;
                string eventDesc = string.Empty;
                GetEventRequestData(status, out eventCode, out eventDesc);

                Hashtable values = new Hashtable();
                values.Add("tpEvento", eventCode);

                if (returnData.DocumentElement.SelectSingleNode("nfens:retEvento/nfens:infEvento/nfens:cStat", ns) == null)
                {
                    string reason = returnData.DocumentElement.SelectSingleNode("nfens:xMotivo", ns).InnerText;

                    string message = string.Format("ManifestNFe: {0}", reason);
                    logger.Write(message);

                    common.SendResponse(nfeKey, 0, reason, values, responseUrl);

                    return new Response<bool>(-1, reason, false);
                }
                else
                {
                    int responseStatus = Convert.ToInt32(returnData.DocumentElement.SelectSingleNode("nfens:retEvento/nfens:infEvento/nfens:cStat", ns).InnerText);
                    string reason = returnData.DocumentElement.SelectSingleNode("nfens:retEvento/nfens:infEvento/nfens:xMotivo", ns).InnerText;

                    if (responseStatus == 135 || responseStatus == 136)
                    {
                        if(notifyOnSuccess)
                            common.SendResponse(nfeKey, responseStatus, reason, values, responseUrl);

                        return new Response<bool>(responseStatus, "Manifesto executado com sucesso.", true);
                    }
                    else
                    {
                        string message = string.Format("ManifestNFe: {0}", reason);
                        logger.Write(message);

                        common.SendResponse(nfeKey, responseStatus, reason, values, responseUrl);

                        return new Response<bool>(responseStatus, reason, false);
                    }
                }
            }
            catch (Exception ex)
            {
                string message = string.Format("ManifestNFe - {0}: {1} - {2}", DateTime.Now.ToString(), ex.Message, ex.StackTrace);

                logger.Write(message);
                return new Response<bool>(-1, message, false);
            }
        }

        private XmlDocument GetRequestData(string cnpj, string nfeKey, ManifestStatus status)
        {
            string eventCode = string.Empty;
            string eventDesc = string.Empty;

            GetEventRequestData(status, out eventCode, out eventDesc);

            // Pegando XML modelo para envio
            XmlDocument template = new XmlDocument();
            using (StreamReader reader = new StreamReader(this.GetType().Assembly.GetManifestResourceStream("DS.NFIntegration.Core.SEFAZ.Recepcaoevento.Template.xml")))
            {
                template.LoadXml(reader.ReadToEnd());
            }

            XmlNamespaceManager ns = new XmlNamespaceManager(template.NameTable);
            ns.AddNamespace("nfens", "http://www.portalfiscal.inf.br/nfe");

            template.DocumentElement.SelectSingleNode("nfens:evento/nfens:infEvento", ns).Attributes["Id"].InnerText = string.Format("ID{0}{1}01", eventCode, nfeKey);
            template.DocumentElement.SelectSingleNode("nfens:evento/nfens:infEvento/nfens:cOrgao", ns).InnerText = "91";
            template.DocumentElement.SelectSingleNode("nfens:evento/nfens:infEvento/nfens:CNPJ", ns).InnerText = cnpj;
            template.DocumentElement.SelectSingleNode("nfens:evento/nfens:infEvento/nfens:chNFe", ns).InnerText = nfeKey;
            template.DocumentElement.SelectSingleNode("nfens:evento/nfens:infEvento/nfens:tpEvento", ns).InnerText = eventCode;
            template.DocumentElement.SelectSingleNode("nfens:evento/nfens:infEvento/nfens:detEvento/nfens:descEvento", ns).InnerText = eventDesc;

            int timeZoneHours = Convert.ToInt32(DateTime.Now.Subtract(DateTime.UtcNow).TotalHours);
            string timeZoneRepresentation = string.Empty;

            if (timeZoneHours >= 0)
                timeZoneRepresentation = string.Format("+{0}:00", timeZoneHours.ToString().PadLeft(2, Convert.ToChar("0")));
            else
                timeZoneRepresentation = string.Format("-{0}:00", (timeZoneHours * -1).ToString().PadLeft(2, Convert.ToChar("0")));

            template.DocumentElement.SelectSingleNode("nfens:evento/nfens:infEvento/nfens:dhEvento", ns).InnerText = string.Format("{0}{1}",
                DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"), timeZoneRepresentation);

            return template;
        }

        private void GetEventRequestData(ManifestStatus status, out string eventCode, out string eventDesc)
        {
            eventCode = string.Empty;
            eventDesc = string.Empty;

            switch (status)
            {
                case ManifestStatus.Confirmed:
                    eventCode = "210200";
                    eventDesc = "Confirmacao da Operacao";
                    break;

                case ManifestStatus.Known:
                    eventCode = "210210";
                    eventDesc = "Ciencia da Operacao";
                    break;

                case ManifestStatus.Refused:
                    eventCode = "210240";
                    eventDesc = "Operacao nao Realizada";
                    break;

                case ManifestStatus.Unknown:
                    eventCode = "210220";
                    eventDesc = "Desconhecimento da Operacao";
                    break;
            }
        }

        private void GetEventRequestData(string eventCode, out ManifestStatus status, out string eventDesc)
        {
            status = ManifestStatus.Pending;
            eventDesc = string.Empty;

            switch (eventCode)
            {
                case "210200":
                    status = ManifestStatus.Confirmed;
                    eventDesc = "Confirmacao da Operacao";
                    break;

                case "210210":
                    status = ManifestStatus.Known;
                    eventDesc = "Ciencia da Operacao";
                    break;

                case "210240":
                    status = ManifestStatus.Refused;
                    eventDesc = "Operacao nao Realizada";
                    break;

                case "210220":
                    status = ManifestStatus.Unknown;
                    eventDesc = "Desconhecimento da Operacao";
                    break;
            }
        }

        private XmlDocument DoManifest(XmlDocument data, X509Certificate2 certificate)
        {
            NFeCustomBinding nfeCustomBinding = new NFeCustomBinding();
            RecepcaoEventoSoap12Client client = new RecepcaoEventoSoap12Client(
                nfeCustomBinding.GetNFeCustomBinding(),
                new System.ServiceModel.EndpointAddress("https://www.nfe.fazenda.gov.br/RecepcaoEvento/RecepcaoEvento.asmx"));

            client.ClientCredentials.ClientCertificate.Certificate = certificate;

            XmlNamespaceManager ns = new XmlNamespaceManager(data.NameTable);
            ns.AddNamespace("nfens", "http://www.portalfiscal.inf.br/nfe");

            // Assinatura /////////////////////////////////////////////////////////////////////
            string id = data.DocumentElement.SelectSingleNode("nfens:evento/nfens:infEvento", ns).Attributes["Id"].InnerText;

            SignedXml signedXml = new SignedXml(data);
            signedXml.SigningKey = certificate.PrivateKey;

            Reference reference = new Reference("#" + id);
            reference.AddTransform(new XmlDsigEnvelopedSignatureTransform());
            reference.AddTransform(new XmlDsigC14NTransform());
            signedXml.AddReference(reference);

            KeyInfo keyInfo = new KeyInfo();
            keyInfo.AddClause(new KeyInfoX509Data(certificate));

            signedXml.KeyInfo = keyInfo;

            signedXml.ComputeSignature();

            XmlElement xmlSignature = data.CreateElement("Signature", "http://www.w3.org/2000/09/xmldsig#");
            XmlElement xmlSignedInfo = signedXml.SignedInfo.GetXml();
            XmlElement xmlKeyInfo = signedXml.KeyInfo.GetXml();

            XmlElement xmlSignatureValue = data.CreateElement("SignatureValue", xmlSignature.NamespaceURI);
            string signBase64 = Convert.ToBase64String(signedXml.Signature.SignatureValue);
            XmlText text = data.CreateTextNode(signBase64);
            xmlSignatureValue.AppendChild(text);

            xmlSignature.AppendChild(data.ImportNode(xmlSignedInfo, true));
            xmlSignature.AppendChild(xmlSignatureValue);
            xmlSignature.AppendChild(data.ImportNode(xmlKeyInfo, true));

            data.DocumentElement.ChildNodes[1].AppendChild(xmlSignature);
            //data.DocumentElement.SelectSingleNode("nfens:evento", ns).AppendChild(xmlSignature);
            ///////////////////////////////////////////////////////////////////////////////////

            nfeCabecMsg cabec = new nfeCabecMsg();
            cabec.versaoDados = "1.00";

            XmlNode returnNode = client.nfeRecepcaoEvento(ref cabec, (XmlNode)(data.DocumentElement));

            XmlDocument returnDoc = new XmlDocument();
            returnDoc.LoadXml(returnNode.OuterXml);

            return returnDoc;
        }
    }
}
