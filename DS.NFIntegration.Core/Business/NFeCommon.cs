﻿using Amazon.S3;
using Amazon.S3.Model;
using DS.NFIntegration.Core.Entities;
using DS.NFIntegration.Core.Interfaces;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Text;
using System.Threading;
using System.Xml;

namespace DS.NFIntegration.Core.Business
{
    internal class NFeCommon : IDisposable
    {
        private AmazonS3Client s3Client = null;
        private ILogger logger = null;

        public NFeCommon(ILogger logger)
        {
            this.logger = logger;
        }

        public XmlDocument UncompressDocument(string content)
        {
            byte[] compressedContent = Convert.FromBase64String(content);
            List<byte> uncompressedContent = new List<byte>();

            using (MemoryStream stream = new MemoryStream(compressedContent))
            {
                using (GZipStream zip = new GZipStream(stream, CompressionMode.Decompress))
                {
                    zip.BaseStream.Seek(0, SeekOrigin.Begin);

                    int singleByte = zip.ReadByte();
                    while (singleByte >= 0)
                    {
                        uncompressedContent.Add(Convert.ToByte(singleByte));
                        singleByte = zip.ReadByte();
                    }
                }
            }

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(Encoding.UTF8.GetString(uncompressedContent.ToArray()));

            return doc;
        }

        public string SaveNFe(string key, XmlDocument nfeDoc, S3AccessConfiguration accessConfiguration)
        {
            if (s3Client == null)
                s3Client = new AmazonS3Client(accessConfiguration.Key, accessConfiguration.Secret,
                    Amazon.RegionEndpoint.GetBySystemName(accessConfiguration.Region));

            string fileName = $"{key}.xml";
            PutObjectResponse resp = null;

            using (MemoryStream ms = new MemoryStream())
            {
                nfeDoc.Save(ms);
                ms.Seek(0, SeekOrigin.Begin);

                PutObjectRequest putReq = new PutObjectRequest();
                putReq.BucketName = accessConfiguration.Bucket;
                putReq.InputStream = ms;
                putReq.Key = accessConfiguration.Path + fileName;

                resp = s3Client.PutObjectAsync(putReq).Result;
            }

            if (resp.HttpStatusCode != System.Net.HttpStatusCode.Accepted &&
                resp.HttpStatusCode != System.Net.HttpStatusCode.Created &&
                resp.HttpStatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"SendToBucket: não foi possível carregar arquivo {fileName} - {resp.HttpStatusCode}.");
            }

            return fileName;
        }

        public string GetInterfyAccessToken()
        {
            if (Globals.TOKEN != null)
            {
                return Globals.TOKEN;
            } else
            {
                //Formatando e validando as credenciais para pegar o token
                Console.WriteLine("Getting Token");
                //Pegando conteudo de uma variavel de ambiente
                string clientId = System.Environment.GetEnvironmentVariable("IFY_CLIENT_ID");
                string clientSecret = System.Environment.GetEnvironmentVariable("IFY_CLIENT_SECRET");
                string apiUrl = System.Environment.GetEnvironmentVariable("IFY_API_URL");

                //string credentials = String.Format("{0}:{1}", clientId, clientSecret);

                try
                {
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl + "/api/v2/oauth/token");

                    var postData = "client_id=" + Uri.EscapeDataString(clientId);
                    postData += "&client_secret=" + Uri.EscapeDataString(clientSecret);
                    postData += "&scope=" + Uri.EscapeDataString("*");
                    postData += "&grant_type=" + Uri.EscapeDataString("client_credentials");
                    var data = Encoding.ASCII.GetBytes(postData);

                    request.Method = "POST";
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.ContentLength = data.Length;
                    request.Headers["X-Workspace"] = Globals.WORKSPACE;

                    using (var stream = request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }

                    var response = (HttpWebResponse)request.GetResponse();

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                        var obj = JsonConvert.DeserializeObject<InterfyAccessToken>(responseString);
                        Globals.TOKEN = obj.access_token;
                        return obj.access_token;
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    if (ex.InnerException == null)
                        logger.Write("AccessTokenError", ex);
                    else
                        logger.Write("AccessTokenError", ex.InnerException);
                }

                return null;
            }
        }

        public void SendResponse(string key, int statusCode, string message, Hashtable otherValues, string url)
        {
            // Aguardando limite de 60 requisições por minuto no Interfy.
            InterfyRequestsControler.WaitForNextRequest(logger);

            try
            {
                var token = GetInterfyAccessToken();

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Headers["Authorization"] = "Bearer " + token;
                request.Headers["X-Workspace"] = Globals.WORKSPACE;

                Hashtable values = new Hashtable();
                values.Add("cStat", statusCode);
                values.Add("xMotivo", message);
                if (otherValues != null && otherValues.Count > 0)
                    foreach (DictionaryEntry item in otherValues)
                        values.Add(item.Key, item.Value);

                Hashtable ht = new Hashtable();
                ht.Add(key, values);

                string json = JsonConvert.SerializeObject(ht);

                using (StreamWriter streamWriter = new StreamWriter(request.GetRequestStreamAsync().Result))
                {
                    streamWriter.Write(json);
                }

                using (WebResponse webResponse = request.GetResponseAsync().Result)
                {
                    // Não é necessário veriricar o conteúdo retornado.
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null) {
                    logger.Write("SendFaultResponse", ex);
                } else if (ex.InnerException is WebException) {
                    var exc = ex.InnerException as WebException;
                    using (HttpWebResponse resp = exc.Response as HttpWebResponse) {
                        using (Stream stream = resp.GetResponseStream()) {
                            StreamReader reader = new StreamReader(stream);
                            string strResponse = reader.ReadToEnd();

                            logger.Write("SendFaultResponse: RESPONSE - " + strResponse, ex.InnerException);
                        }
                    }
                } else {
                    logger.Write("SendFaultResponse", ex.InnerException);
                }
            }
            finally
            {
                InterfyRequestsControler.SetRequest();
            }
        }

        public void SendFaultResponse(string message, string url)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                request.ContentType = "application/json";

                Hashtable values = new Hashtable();
                values.Add("cStat", -1);
                values.Add("xMotivo", message);

                Hashtable ht = new Hashtable();
                ht.Add("ERROR", values);

                string json = JsonConvert.SerializeObject(ht);

                using (StreamWriter streamWriter = new StreamWriter(request.GetRequestStreamAsync().Result))
                {
                    streamWriter.Write(json);
                }

                using (WebResponse webResponse = request.GetResponseAsync().Result)
                {
                    // Não é necessário veriricar o conteúdo retornado.
                }
            }
            catch (Exception ex)
            {
                logger.Write("SendFaultResponse", ex);
            }
        }

        public void Dispose()
        {
            if (s3Client != null)
            {
                s3Client.Dispose();
                s3Client = null;
            }

            GC.SuppressFinalize(this);
        }

        private static class InterfyRequestsControler
        {
            private static int interfyMaxRequestsPerMinute = 0;
            private static List<DateTime> lastMinuteRequests = new List<DateTime>();
            private static object myLock = new object();
            
            public static void WaitForNextRequest(ILogger logger)
            {
                lock (myLock)
                {
                    //Configuração.
                    SetConfiguration(logger);

                    // Removo o que já saiu do 1 minuto.
                    while (lastMinuteRequests.Count > 0 && lastMinuteRequests[0] < DateTime.Now.AddMinutes(-1))
                        lastMinuteRequests.RemoveAt(0);

                    // Aguardando o último
                    while (lastMinuteRequests.Count >= interfyMaxRequestsPerMinute && lastMinuteRequests[0] > DateTime.Now.AddMinutes(-1))
                    {
                        // Aguardo 2 segundos.
                        logger.Write("WaitForNextRequest - Aguardando 2 segundos para não ultrapassar limite de requests para o Interfy...");
                        AutoResetEvent are = new AutoResetEvent(false);
                        are.WaitOne(2000);

                        // Removo o que já saiu do 1 minuto.
                        while (lastMinuteRequests.Count > 0 && lastMinuteRequests[0] < DateTime.Now.AddMinutes(-1))
                            lastMinuteRequests.RemoveAt(0);
                    }
                }

                return;
            }

            public static void SetRequest()
            {
                // Adiciona informação de que novo request foi efetuado.
                lastMinuteRequests.Add(DateTime.Now);
            }

            private static void SetConfiguration(ILogger logger)
            {
                try
                {
                    if (interfyMaxRequestsPerMinute <= 0)
                    {
                        IConfigurationBuilder builder = new ConfigurationBuilder()
                            .SetBasePath(Directory.GetCurrentDirectory())
                            .AddJsonFile("appsettings.json");

                        interfyMaxRequestsPerMinute = Convert.ToInt32(builder.Build()["interfyMaxRequestsPerMinute"]);

                        if (interfyMaxRequestsPerMinute <= 0)
                        {
                            interfyMaxRequestsPerMinute = 60;
                            logger.Write("SetConfiguration - configuração inválida para limite de chamadas por minuto. Será utilizado o valor padrão 60.");
                        }
                    }
                }
                catch (Exception ex)
                {
                    interfyMaxRequestsPerMinute = 60;
                    logger.Write("SetConfiguration - não foi possível pegar configuração para limite de chamadas por minuto. Será utilizado o valor padrão 60.", ex);
                }
            }
        }
    }
}
