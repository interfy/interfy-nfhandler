﻿using Amazon.Lambda.Core;
using DS.NFIntegration.Core.Interfaces;
using System;

namespace DS.NFIntegration.Core.Business
{
    /// <summary>
    /// Mecanismo de log
    /// </summary>
    public class LambdaLogger : ILogger
    {
        ILambdaLogger logger = null;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="logger">Mecanismo de log do Lambda</param>
        public LambdaLogger(ILambdaLogger logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// Registra uma mensagem. 
        /// </summary>
        /// <param name="message">Mensagem</param>
        public void Write(string message)
        {
            if (logger != null)
                logger.LogLine(message);
        }

        /// <summary>
        /// Registra um erro.
        /// </summary>
        /// <param name="message">Mensagem personalizada</param>
        /// <param name="ex">Erro</param>
        public void Write(string message, Exception ex)
        {
            if (ex == null)
            {
                Write(message);
            }
            else
            {
                if (logger != null)
                {
                    message = ex.InnerException == null ? ex.Message : ex.InnerException.Message;
                    Write(message);
                }
            }
        }
    }
}
