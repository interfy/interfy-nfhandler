﻿using DS.NFIntegration.Core.Entities;
using System;
using System.Xml;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using DS.NFIntegration.SEFAZ.NFeDistribuicaoDFe;
using System.Collections;
using DS.NFIntegration.Core.Interfaces;

namespace DS.NFIntegration.Core.Business
{
    /// <summary>
    /// Esta classe é responsável por buscar, baixar e armazenar novas notas.
    /// </summary>
    public class NFListRetriever
    {
        private ILogger logger = null;

        /// <summary>
        /// Construtor
        /// </summary>
        /// <param name="logger">Logger do Lambda</param>
        public NFListRetriever(ILogger logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// Carrega as notas disponíveis desde o último processamento.
        /// </summary>
        /// <param name="cnpj">CNPJ</param>
        /// <param name="acknowledge">Indica se é para dar ciência em notas não manifestadas</param>
        /// <param name="lastNSU">Último NSU</param>
        /// <param name="accessConfiguration">Acesso ao bucket onde o XML deverá ser armazenado</param>
        /// <param name="responseUrl">Url de retorno para POST com resultado</param>
        /// <param name="certificate">Certificado</param>
        /// <returns>Last NSU</returns>
        public int Retrieve(string cnpj, bool acknowledge, int lastNSU, S3AccessConfiguration accessConfiguration, string responseUrl, X509Certificate2 certificate)
        {
            try
            {
                logger.Write("Retrieve - Processamento iniciado...");

                // Fluxo:
                //      - Verificar último código retornado
                //      - Baixar novas notas
                //      - Para notas manifestadas
                //          - Gravar XML no S3
                //          - Chamar API Interfy
                //      - Para notas não manifestadas
                //          - Se acknowledge -> dar ciência
                //          - Gravar XML no S3
                //          - Chamar API Interfy

                using (NFeCommon common = new NFeCommon(logger))
                {
                    int lastReturnedNSU = 0;
                    Dictionary<string, XmlDocument> nfePartialDocs = GetNextDocuments(cnpj, lastNSU, certificate, common, logger, out lastReturnedNSU);
                    logger.Write(string.Format("Retrieve - Retornada(s) {0} nota(s) para o CNPJ {1}.", nfePartialDocs.Count, cnpj));

                    if(nfePartialDocs.Count == 0)
                    {
                        lastNSU = lastReturnedNSU;
                    }
                    else
                    {
                        Manifest manifest = new Manifest(cnpj, certificate, responseUrl, logger);
                        NFDownloader downloader = new NFDownloader(logger);

                        foreach (KeyValuePair<string, XmlDocument> entry in nfePartialDocs)
                        {
                            XmlNamespaceManager ns = new XmlNamespaceManager(entry.Value.NameTable);
                            ns.AddNamespace("nfens", "http://www.portalfiscal.inf.br/nfe");

                            string[] keyData = entry.Key.Split(new char[] { Convert.ToChar("_") }, StringSplitOptions.RemoveEmptyEntries);

                            XmlDocument nfeDownloadAttempt = null;
                            string key = keyData[1];
                            int nsu = Convert.ToInt32(keyData[0]);
                            string status = string.Empty;
                            string reason = string.Empty;

                            // Atualizando lastNSU para retorno.
                            lastNSU = nsu;

                            if (entry.Value.DocumentElement.SelectSingleNode("//nfens:infNFe/@Id", ns) == null)
                            {
                                // Veio a nota parcial
                                XmlNode createdDateNode = entry.Value.DocumentElement.SelectSingleNode("nfens:dhEmi", ns);
                                if (createdDateNode != null)
                                {
                                    DateTime createdOn = DateTime.ParseExact(createdDateNode.InnerText.Substring(0, 10), "yyyy-MM-dd", null);

                                    nfeDownloadAttempt = downloader.DownloadNFe(key, createdOn, cnpj, certificate, out status, out reason);
                                    if (nfeDownloadAttempt == null)
                                        continue;

                                    // Se precisa fazer o manifesto, verifico se pode fazer a ciência automaticamente.
                                    if (status == "633" && acknowledge)
                                    {
                                        Response<bool> manifestResponse = manifest.ManifestNFe(ManifestStatus.Known, key, false);
                                        if (manifestResponse.Data)
                                            nfeDownloadAttempt = downloader.DownloadNFe(key, createdOn, cnpj, certificate, out status, out status);
                                        else
                                            continue;
                                    }
                                }
                            }
                            else
                            {
                                // Veio a nota completa
                                nfeDownloadAttempt = entry.Value;
                                status = "138";
                                reason = "Documento localizado";
                            }

                            try
                            {
                                string fileName = string.Empty;

                                if (nfeDownloadAttempt == null)
                                    fileName = common.SaveNFe(key, entry.Value, accessConfiguration);
                                else
                                    fileName = common.SaveNFe(key, nfeDownloadAttempt, accessConfiguration);

                                Hashtable values = new Hashtable();
                                values.Add("file", fileName);
                                values.Add("nsu", nsu);
                                common.SendResponse(key, Convert.ToInt32(status), reason, values, responseUrl);
                            }
                            catch (Exception ex)
                            {
                                string message = string.Format("Retrieve - Erro salvando nota {0} do CNPJ {1}: {2}", key, cnpj, ex.Message);

                                logger.Write(message);
                                common.SendResponse(key, -1, message, null, responseUrl);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (logger != null)
                    logger.Write("Retrieve - Erro não tratado", ex);

                new NFeCommon(logger).SendFaultResponse(string.Format("Retrieve - Erro não tratado: {0}", ex.Message), responseUrl);
            }
            finally
            {
                if (logger != null)
                    logger.Write("Retrieve - Processamento finalizado.");
            }

            return lastNSU;
        }

        private Dictionary<string, XmlDocument> GetNextDocuments(string cnpj, int lastNSU, X509Certificate2 certificate, NFeCommon common, ILogger log, out int lastReturnedNSU)
        {
            lastReturnedNSU = 0;

            NFeCustomBinding nfeCustomBinding = new NFeCustomBinding();
            NFeDistribuicaoDFeSoapClient client = new NFeDistribuicaoDFeSoapClient(
                nfeCustomBinding.GetNFeCustomBinding(),
                nfeCustomBinding.GetEndpointAddress());

            client.ChannelFactory.Credentials.ServiceCertificate.SslCertificateAuthentication = new System.ServiceModel.Security.X509ServiceCertificateAuthentication()
            {
                CertificateValidationMode = System.ServiceModel.Security.X509CertificateValidationMode.None,
                RevocationMode = System.Security.Cryptography.X509Certificates.X509RevocationMode.NoCheck
            };

            client.ClientCredentials.ClientCertificate.Certificate = certificate;

            // Pegando XML modelo para envio
            XmlDocument template = new XmlDocument();
            using (StreamReader reader = new StreamReader(this.GetType().Assembly.GetManifestResourceStream("DS.NFIntegration.Core.SEFAZ.NFeDistribuicaoDFe.Template.xml")))
            {
                template.LoadXml(reader.ReadToEnd());
            }

            XmlNamespaceManager ns = new XmlNamespaceManager(template.NameTable);
            ns.AddNamespace("nfens", "http://www.portalfiscal.inf.br/nfe");

            template.DocumentElement.SelectSingleNode("nfens:CNPJ", ns).InnerText = cnpj;
            template.DocumentElement.SelectSingleNode("nfens:distNSU/nfens:ultNSU", ns).InnerText = lastNSU.ToString().PadLeft(15, Convert.ToChar("0"));

            XmlDocument returnDocument = null;

            try
            {
                XmlElement returnElement = client.nfeDistDFeInteresse(template.DocumentElement);

                if (returnElement == null)
                    return new Dictionary<string, XmlDocument>();

                returnDocument = new XmlDocument();
                returnDocument.LoadXml(returnElement.OuterXml);
            }
            catch (Exception ex)
            {
                log.Write(string.Format("GetNextDocuments - Erro obtendo lista de notas para CNPJ {0}", cnpj), ex);
                return new Dictionary<string, XmlDocument>();
            }

            ns = new XmlNamespaceManager(returnDocument.NameTable);
            ns.AddNamespace("nfens", "http://www.portalfiscal.inf.br/nfe");

            XmlNodeList docs = returnDocument.DocumentElement.SelectNodes("nfens:loteDistDFeInt/nfens:docZip", ns);
            if (docs == null || docs.Count == 0)
            {
                XmlNode reason = returnDocument.DocumentElement.SelectSingleNode("nfens:xMotivo", ns);
                if (reason != null)
                {
                    log.Write(string.Format("Retrieve - Nenhuma nota retornada para CNPJ {0}: {1}", cnpj, reason.InnerText));
                    return new Dictionary<string, XmlDocument>();
                }
            }

            Dictionary<string, XmlDocument> nfes = new Dictionary<string, XmlDocument>(docs.Count);

            foreach (XmlNode doc in docs)
            {
                // Guardando o último NSU
                lastReturnedNSU = Convert.ToInt32(doc.Attributes["NSU"].InnerText);

                // Descompactando e guardando o XML parcial da NFe (o XML completo vem depois do manifesto)
                XmlDocument partialNFe = common.UncompressDocument(doc.InnerText);

                // Verificando se é nota ou algum outro evento
                if (doc.Attributes["schema"].InnerText.StartsWith("resNFe") || doc.Attributes["schema"].InnerText.StartsWith("procNFe"))
                {
                    XmlNamespaceManager ns2 = new XmlNamespaceManager(partialNFe.NameTable);
                    ns2.AddNamespace("nfens", "http://www.portalfiscal.inf.br/nfe");

                    if (doc.Attributes["schema"].InnerText.StartsWith("resNFe") || doc.Attributes["schema"].InnerText.StartsWith("procNFe"))
                    {
                        // Chave da nota
                        string key = lastReturnedNSU.ToString() + "_" + partialNFe.DocumentElement.SelectSingleNode("//nfens:chNFe", ns).InnerText;

                        // Se não está na lista, adiciono. Se já está atualizo.
                        if (!nfes.ContainsKey(key))
                            nfes.Add(key, partialNFe);
                        else if (doc.Attributes["schema"].InnerText.StartsWith("procNFe"))
                            nfes[key] = partialNFe;
                    }
                }
            }

            return nfes;
        }
    }
}
