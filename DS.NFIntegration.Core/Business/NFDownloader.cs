﻿using DS.NFIntegration.Core.Interfaces;
using DS.NFIntegration.SEFAZ.NFeDistribuicaoDFe;
using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Xml;

namespace DS.NFIntegration.Core.Business
{
    /// <summary>
    /// Classe de download de notas
    /// </summary>
    internal class NFDownloader
    {
        private ILogger logger = null;

        public NFDownloader(ILogger logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// Tenta baixar NFe. Caso ainda não tenha sido feito o manifesto, não será possível baixar, e será retornado null.
        /// </summary>
        /// <param name="key">Chave da NFe</param>
        /// <param name="cnpj">CNPJ</param>
        /// <param name="certificate">Certificado para chamada ao WS da SEFAZ</param>
        /// <param name="status">Status retornado pela SEFAZ</param>
        /// <param name="reason">Motivo para descarte (preenchido apenas se discard == true)</param>
        /// <returns>XmlDocument</returns>
        public XmlDocument DownloadNFe(string key, string cnpj, X509Certificate2 certificate,
            out string status, out string reason)
        {
            status = string.Empty;
            reason = string.Empty;

            try
            {
                return DownloadNFe(key, DateTime.MinValue, cnpj, certificate, out status, out reason);
            }
            catch (Exception ex)
            {
                logger.Write(string.Format("DownloadNFe - Erro baixando nota para CNPJ {0}", cnpj), ex);
                return null;
            }
        }

        /// <summary>
        /// Tenta baixar NFe. Caso ainda não tenha sido feito o manifesto, não será possível baixar, e será retornado o XML parcial.
        /// </summary>
        /// <param name="key">Chave da NFe</param>
        /// <param name="createdOn">Data de emissão da nota (apenas para log)</param>
        /// <param name="cnpj">CNPJ</param>
        /// <param name="certificate">Certificado para chamada ao WS da SEFAZ</param>
        /// <param name="status">Status retornado pela SEFAZ</param>
        /// <param name="reason">Motivo para descarte (preenchido apenas se discard == true)</param>
        /// <returns>XmlDocument</returns>
        internal XmlDocument DownloadNFe(string key, DateTime createdOn, string cnpj, X509Certificate2 certificate,
            out string status, out string reason)
        {
            status = string.Empty;
            reason = string.Empty;

            try
            {
                NFeCustomBinding nfeCustomBinding = new NFeCustomBinding();
                NFeDistribuicaoDFeSoapClient client = new NFeDistribuicaoDFeSoapClient(
                    nfeCustomBinding.GetNFeCustomBinding(),
                    nfeCustomBinding.GetEndpointAddress());

                client.ChannelFactory.Credentials.ServiceCertificate.SslCertificateAuthentication = new System.ServiceModel.Security.X509ServiceCertificateAuthentication()
                {
                    CertificateValidationMode = System.ServiceModel.Security.X509CertificateValidationMode.None,
                    RevocationMode = System.Security.Cryptography.X509Certificates.X509RevocationMode.NoCheck
                };

                client.ClientCredentials.ClientCertificate.Certificate = certificate;

                // Pegando XML modelo para envio
                XmlDocument template = new XmlDocument();
                using (StreamReader reader = new StreamReader(this.GetType().Assembly.GetManifestResourceStream("DS.NFIntegration.Core.SEFAZ.NFeDistribuicaoDFe.Template_Download.xml")))
                {
                    template.LoadXml(reader.ReadToEnd());
                }

                XmlNamespaceManager ns = new XmlNamespaceManager(template.NameTable);
                ns.AddNamespace("nfens", "http://www.portalfiscal.inf.br/nfe");

                template.DocumentElement.SelectSingleNode("nfens:CNPJ", ns).InnerText = cnpj;
                template.DocumentElement.SelectSingleNode("nfens:consChNFe/nfens:chNFe", ns).InnerText = key;

                XmlDocument returnDocument = null;
                
                try
                {
                    XmlElement returnElement = client.nfeDistDFeInteresse(template.DocumentElement);

                    if (returnElement == null)
                        return null;

                    returnDocument = new XmlDocument();
                    returnDocument.LoadXml(returnElement.OuterXml);
                }
                catch (Exception ex)
                {
                    logger.Write(string.Format("DownloadNFe - Erro obtendo nota {0}", key), ex);
                    return null;
                }

                ns = new XmlNamespaceManager(returnDocument.NameTable);
                ns.AddNamespace("nfens", "http://www.portalfiscal.inf.br/nfe");

                XmlNodeList docs = returnDocument.DocumentElement.SelectNodes("nfens:loteDistDFeInt/nfens:docZip", ns);
                status = returnDocument.DocumentElement.SelectSingleNode("//nfens:cStat", ns).InnerText;
                reason = returnDocument.DocumentElement.SelectSingleNode("//nfens:xMotivo", ns).InnerText;

                if (status == "138" && docs != null && docs.Count > 0)
                {
                    // Download disponibilizado.
                    foreach (XmlNode doc in docs)
                    {
                        // Descompactando e guardando o XML parcial da NFe (o XML completo vem depois do manifesto).
                        XmlDocument nfeDoc = new NFeCommon(logger).UncompressDocument(doc.InnerText);

                        // Verificando se é nota ou algum outro evento.
                        if (doc.Attributes["schema"].InnerText.StartsWith("resNFe") || doc.Attributes["schema"].InnerText.StartsWith("procNFe"))
                            return nfeDoc;
                    }
                }

                // Se chegou até aqui, retorno o último evento disponível.
                return new NFeCommon(logger).UncompressDocument(docs[docs.Count - 1].InnerText);
            }
            catch (Exception ex)
            {
                logger.Write(string.Format("DownloadNFe - Erro baixando nota para CNPJ {0}", cnpj), ex);
                return null;
            }
        }
    }
}
