﻿using DS.NFIntegration.Core.Interfaces;
using System;
using System.IO;

namespace DS.NFIntegration.Core.Business
{
    /// <summary>
    /// Mecanismo de log em arquivo
    /// </summary>
    public class FileLogger : ILogger
    {
        /// <summary>
        /// Registra uma mensagem. 
        /// </summary>
        /// <param name="message">Mensagem</param>
        public void Write(string message)
        {
            if (string.IsNullOrEmpty(message))
                return;

            string path = GetPath();
            File.AppendAllLines(path, new string[] { $"{DateTime.Now.ToString("HH:mm:ss")}: {message}" });

            Console.WriteLine(message);
        }

        /// <summary>
        /// Registra um erro.
        /// </summary>
        /// <param name="message">Mensagem personalizada</param>
        /// <param name="ex">Erro</param>
        public void Write(string message, Exception ex)
        {
            if (ex == null)
                return;

            message = string.IsNullOrEmpty(message) ? string.Empty : message;

            string path = GetPath();
            File.AppendAllLines(path, new string[] { $"{DateTime.Now.ToString("HH:mm:ss")}: {message} - {ex.Message} - {ex.StackTrace}" });

            Console.WriteLine($"{message} - {ex.Message}");
        }

        private string GetPath()
        {
            string path = Path.Combine(Directory.GetCurrentDirectory(), "Log");
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            return Path.Combine(path, $"Log_{DateTime.Now.ToString("yyyy-MM-dd")}.txt");
        }
    }
}
