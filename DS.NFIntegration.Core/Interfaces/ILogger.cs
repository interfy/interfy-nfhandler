﻿using System;

namespace DS.NFIntegration.Core.Interfaces
{
    /// <summary>
    /// Interface para mecanismo de log
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// Registra uma mensagem. 
        /// </summary>
        /// <param name="message">Mensagem</param>
        void Write(string message);

        /// <summary>
        /// Registra um erro.
        /// </summary>
        /// <param name="message">Mensagem personalizada</param>
        /// <param name="ex">Erro</param>
        void Write(string message, Exception ex);
    }
}
