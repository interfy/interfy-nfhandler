using Amazon.Lambda.Core;
using Amazon.Lambda.SNSEvents;
using Amazon.SQS;
using Amazon.SQS.Model;
using DS.NFIntegration.Core.Business;
using DS.NFIntegration.Core.Entities;
using DS.NFIntegration.Core.Interfaces;
using DS.NFIntegration.Lambda.Entities;
using Newtonsoft.Json;
using System;
using System.Security.Cryptography.X509Certificates;
using static Amazon.Lambda.SNSEvents.SNSEvent;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace DS.NFIntegration.Lambda
{
    public class Function
    {
        /// <summary>
        /// M�todo acionado pelo Cloudwatch para busca de informa��es em uma fila (SQS)
        /// e posterior processamento de NFe.
        /// </summary>
        /// <param name="context">Contexto</param>
        /// <returns>string</returns>
        public string SQSHandler(ILambdaContext context)
        {
            ILogger logger = null;
            if (context != null && context.Logger != null)
                logger = new Core.Business.LambdaLogger(context.Logger);

            Log("In�cio do processamento.", logger);

            string from = Environment.GetEnvironmentVariable("fromQueue");
            string to = Environment.GetEnvironmentVariable("toQueue");

            if (string.IsNullOrEmpty(from))
                Log("Chave fromQueue n�o encontrada.", logger);
            else
                Log("Chave(s) encontrada(s). Prosseguindo com processamento.", logger);

            using (AmazonSQSClient client = new AmazonSQSClient())
            {
                SendMessageResponse resp = null;

                ReceiveMessageResponse receiveMessage = client.ReceiveMessageAsync(from).Result;
                Log($"Recebidas {receiveMessage.Messages.Count} mensagens.", logger);

                foreach (Message msg in receiveMessage.Messages)
                {
                    try
                    {
                        BaseRequest baseRequest = JsonConvert.DeserializeObject<BaseRequest>(msg.Body);
                        X509Certificate2 certificate = baseRequest.GetCertificate(logger);
                        if (certificate == null)
                        {
                            Log("Processamento finalizado.", logger);
                            return string.Empty;
                        }

                        if (baseRequest.Action == "download")
                        {
                            DownloadRequest downloadRequest = JsonConvert.DeserializeObject<DownloadRequest>(msg.Body);
                            NFListRetriever retriever = new NFListRetriever(logger);

                            string s3Key = Environment.GetEnvironmentVariable("s3_key");
                            string s3Secret = Environment.GetEnvironmentVariable("s3_secret");
                            string s3Region = Environment.GetEnvironmentVariable("s3_region");

                            if (string.IsNullOrEmpty(s3Key) || string.IsNullOrEmpty(s3Secret) || string.IsNullOrEmpty(s3Region))
                            {
                                Log("N�o encontradas informa��es de acesso ao S3.", logger);
                            }
                            else
                            {
                                S3AccessConfiguration s3Access = new S3AccessConfiguration(s3Key, s3Secret, s3Region, downloadRequest.Bucket);

                                int lastNSU = retriever.Retrieve(downloadRequest.CNPJ, downloadRequest.Acknowledge, downloadRequest.NSU,
                                    s3Access, downloadRequest.ResponseUri, certificate);

                                if (lastNSU > downloadRequest.NSU)
                                {
                                    Log($"�ltimo NSU ({lastNSU}) maior do que o inicial ({downloadRequest.NSU}). Reenviando para fila.", logger);
                                    downloadRequest.NSU = lastNSU;

                                    try
                                    {
                                        SendMessageRequest req = new SendMessageRequest(from, JsonConvert.SerializeObject(downloadRequest));
                                        req.MessageGroupId = "1";
                                        req.MessageDeduplicationId = DateTime.Now.Ticks.ToString();

                                        resp = client.SendMessageAsync(req).Result;
                                        Log("Mensagem reenviada para fila.", logger);
                                    }
                                    catch (Exception ex)
                                    {
                                        if (ex.InnerException == null)
                                            Log($"Erro reenviando para fila: {ex.Message}", logger);
                                        else
                                            Log($"Erro reenviando para fila: {ex.InnerException.Message}", logger);
                                    }
                                }
                            }
                        }
                        else if (baseRequest.Action == "manifest")
                        {
                            ManifestRequest manifestRequest = JsonConvert.DeserializeObject<ManifestRequest>(msg.Body);
                            Manifest manifest = new Manifest(manifestRequest.CNPJ, certificate, manifestRequest.ResponseUri, logger);

                            foreach (DocToManifest doc in manifestRequest.DocsToManifest)
                                manifest.ManifestNFe(doc.EventCode, doc.Key, true);
                        }
                    }
                    catch (Exception ex)
                    {
                        Log($"Erro processando mensagem: {ex.Message}", logger);
                        continue;
                    }

                    if (!string.IsNullOrEmpty(to))
                    {
                        try
                        {
                            SendMessageRequest req = new SendMessageRequest(to, msg.Body);
                            req.MessageGroupId = "1";
                            req.MessageDeduplicationId = msg.MessageId;

                            resp = client.SendMessageAsync(req).Result;
                            Log("Enviada mensagem para fila.", logger);
                        }
                        catch (Exception ex)
                        {
                            if (ex.InnerException == null)
                                Log($"Erro enviando para fila: {ex.Message}", logger);
                            else
                                Log($"Erro enviando para fila: {ex.InnerException.Message}", logger);
                        }
                    }

                    try
                    {
                        client.DeleteMessageAsync(from, msg.ReceiptHandle);
                        Log("Mensagem original apagada", logger);
                    }
                    catch (Exception ex)
                    {
                        if (ex.InnerException == null)
                            Log($"Erro apagando mensagem: {ex.Message}", logger);
                        else
                            Log($"Erro apagando mensagem: {ex.InnerException.Message}", logger);
                    }
                }
            }

            Log("Processamento finalizado.", logger);

            return string.Empty;
        }

        /// <summary>
        /// M�todo acionado pelo SNS para processamento de NFe.
        /// </summary>
        /// <param name="message">Mensagens</param>
        /// <param name="context">Contexto</param>
        public void SNSEventHandler(SNSEvent snsEvent, ILambdaContext context)
        {
            foreach (SNSRecord record in snsEvent.Records)
                SNSMessageHandler(record.Sns, context);
        }

        /// <summary>
        /// M�todo acionado pelo SNS para processamento de NFe.
        /// </summary>
        /// <param name="message">Mensagem</param>
        /// <param name="context">Contexto</param>
        private void SNSMessageHandler(SNSMessage message, ILambdaContext context)
        {
            ILogger logger = null;
            if (context != null && context.Logger != null)
                logger = new Core.Business.LambdaLogger(context.Logger);

            Log("In�cio do processamento.", logger);
            
            try
            {
                //Log($"Mensagem recebida: {message.Message}.", context);

                BaseRequest baseRequest = JsonConvert.DeserializeObject<BaseRequest>(message.Message);
                X509Certificate2 certificate = baseRequest.GetCertificate(logger);
                if (certificate == null)
                {
                    Log("Processamento finalizado.", logger);
                    return;
                }

                if (baseRequest.Action == "download")
                {
                    DownloadRequest downloadRequest = JsonConvert.DeserializeObject<DownloadRequest>(message.Message);
                    NFListRetriever retriever = new NFListRetriever(logger);

                    string s3Key = Environment.GetEnvironmentVariable("s3_key");
                    string s3Secret = Environment.GetEnvironmentVariable("s3_secret");
                    string s3Region = Environment.GetEnvironmentVariable("s3_region");

                    if (string.IsNullOrEmpty(s3Key) || string.IsNullOrEmpty(s3Secret) || string.IsNullOrEmpty(s3Region))
                    {
                        Log("N�o encontradas informa��es de acesso ao S3.", logger);
                    }
                    else
                    {
                        S3AccessConfiguration s3Access = new S3AccessConfiguration(s3Key, s3Secret, s3Region, downloadRequest.Bucket);

                        // In�cio das chamadas para a SEFAZ.
                        DateTime start = DateTime.Now;

                        int lastNSU = retriever.Retrieve(downloadRequest.CNPJ, downloadRequest.Acknowledge, downloadRequest.NSU,
                            s3Access, downloadRequest.ResponseUri, certificate);

                        // Fim das chamadas para a SEFAZ.
                        DateTime finish = DateTime.Now;

                        decimal requiredTime = Convert.ToDecimal(finish.Subtract(start).Ticks) * 1.25m;

                        // Continuo buscando novas notas enquanto estiver retornando novas informa��es
                        // e ainda houver pelo menos o mesmo tempo restando que o maior processamento + 25%.
                        while (lastNSU > downloadRequest.NSU && Convert.ToDecimal(context.RemainingTime.Ticks) > requiredTime)
                        {
                            Log("Notas retornadas e tempo suficiente. Processando novamente.", logger);

                            // Atualizando o NSU para continuar a partir da �ltima nota baixada.
                            downloadRequest.NSU = lastNSU;

                            // In�cio das chamadas para a SEFAZ.
                            start = DateTime.Now;

                            lastNSU = retriever.Retrieve(downloadRequest.CNPJ, downloadRequest.Acknowledge, downloadRequest.NSU,
                                s3Access, downloadRequest.ResponseUri, certificate);

                            // Fim das chamadas para a SEFAZ.
                            finish = DateTime.Now;

                            decimal temp = Convert.ToDecimal(finish.Subtract(start).Ticks) * 1.25m;
                            if (temp > requiredTime)
                                requiredTime = temp;
                        }
                    }
                }
                else if (baseRequest.Action == "manifest")
                {
                    ManifestRequest manifestRequest = JsonConvert.DeserializeObject<ManifestRequest>(message.Message);
                    Manifest manifest = new Manifest(manifestRequest.CNPJ, certificate, manifestRequest.ResponseUri, logger);

                    foreach (DocToManifest doc in manifestRequest.DocsToManifest)
                        manifest.ManifestNFe(doc.EventCode, doc.Key, true);
                }
            }
            catch (Exception ex)
            {
                Log($"Erro processando mensagem: {ex.Message} - {ex.StackTrace}", logger);
            }            

            Log("Processamento finalizado.", logger);
        }

        private void Log(string msg, ILogger logger)
        {
            if (logger != null)
                logger.Write(msg);
        }
    }
}
