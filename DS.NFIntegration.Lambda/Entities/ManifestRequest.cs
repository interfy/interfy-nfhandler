﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace DS.NFIntegration.Lambda.Entities
{
    public class ManifestRequest : BaseRequest
    {
        [JsonProperty(PropertyName = "docs")]
        public List<DocToManifest> DocsToManifest { get; set; }
    }
}
